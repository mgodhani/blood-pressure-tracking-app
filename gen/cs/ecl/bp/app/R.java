/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package cs.ecl.bp.app;

public final class R {
    public static final class array {
        public static final int bodyPart_array=0x7f070003;
        public static final int gender_array=0x7f070002;
        public static final int height_array=0x7f070001;
        public static final int posture_array=0x7f070004;
        public static final int relation_array=0x7f070005;
        public static final int weight_array=0x7f070000;
    }
    public static final class attr {
        /** <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cameraBearing=0x7f010001;
        /** <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cameraTargetLat=0x7f010002;
        /** <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cameraTargetLng=0x7f010003;
        /** <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cameraTilt=0x7f010004;
        /** <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cameraZoom=0x7f010005;
        /** <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>none</code></td><td>0</td><td></td></tr>
<tr><td><code>normal</code></td><td>1</td><td></td></tr>
<tr><td><code>satellite</code></td><td>2</td><td></td></tr>
<tr><td><code>terrain</code></td><td>3</td><td></td></tr>
<tr><td><code>hybrid</code></td><td>4</td><td></td></tr>
</table>
         */
        public static final int mapType=0x7f010000;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int uiCompass=0x7f010006;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int uiRotateGestures=0x7f010007;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int uiScrollGestures=0x7f010008;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int uiTiltGestures=0x7f010009;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int uiZoomControls=0x7f01000a;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int uiZoomGestures=0x7f01000b;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int useViewLifecycle=0x7f01000c;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int zOrderOnTop=0x7f01000d;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int add=0x7f020000;
        public static final int bar_background=0x7f020001;
        public static final int contact=0x7f020002;
        public static final int content_new=0x7f020003;
        public static final int cross=0x7f020004;
        public static final int doctor=0x7f020005;
        public static final int fail=0x7f020006;
        public static final int groupbox=0x7f020007;
        public static final int groupboxred=0x7f020008;
        public static final int hospital=0x7f020009;
        public static final int ic_launcher=0x7f02000a;
        public static final int ic_user_profile=0x7f02000b;
        public static final int icon_contact_tab=0x7f02000c;
        public static final int icon_profile_tab=0x7f02000d;
        public static final int icon_settings_tab=0x7f02000e;
        public static final int location=0x7f02000f;
        public static final int loginbtn_background=0x7f020010;
        public static final int logo=0x7f020011;
        public static final int logo_balance=0x7f020012;
        public static final int logo_chart=0x7f020013;
        public static final int logo_ecg=0x7f020014;
        public static final int logout=0x7f020015;
        public static final int main_banner=0x7f020016;
        public static final int main_banner2=0x7f020017;
        public static final int mark_blue=0x7f020018;
        public static final int mark_red=0x7f020019;
        public static final int profile=0x7f02001a;
        public static final int settings=0x7f02001b;
        public static final int success=0x7f02001c;
        public static final int tab_bg_selected=0x7f02001d;
        public static final int tab_bg_selector=0x7f02001e;
        public static final int tab_bg_unselected=0x7f02001f;
        public static final int tab_divider=0x7f020020;
        public static final int tab_text_selector=0x7f020021;
        public static final int tip=0x7f020022;
        public static final int user=0x7f020023;
        public static final int weight=0x7f020024;
    }
    public static final class id {
        public static final int LinearLayout1=0x7f04000f;
        public static final int TableLayout1=0x7f040010;
        public static final int add_bpdata=0x7f040023;
        public static final int add_data=0x7f040074;
        public static final int add_emergency=0x7f04003e;
        public static final int address=0x7f04006d;
        public static final int address_add_emerg=0x7f040027;
        public static final int address_emerg_add=0x7f04003d;
        public static final int address_emerg_update=0x7f04004a;
        public static final int address_update_emerg=0x7f040043;
        public static final int advice=0x7f040009;
        public static final int analysis=0x7f040056;
        public static final int barDisplay=0x7f04000b;
        public static final int bodypart=0x7f040020;
        public static final int btnAnalysis=0x7f04000e;
        public static final int btn_show_map=0x7f040011;
        public static final int buttonNext=0x7f04000c;
        public static final int buttonPrevious=0x7f04000d;
        public static final int cancel_addemergcontact=0x7f04002d;
        public static final int comment=0x7f040058;
        public static final int dateRange=0x7f040005;
        public static final int diastolic=0x7f04001c;
        public static final int dob_add=0x7f04006c;
        public static final int dob_update=0x7f040026;
        public static final int emergency=0x7f040061;
        public static final int firstname=0x7f040060;
        public static final int firstname_add=0x7f040063;
        public static final int firstname_add_emerg=0x7f040029;
        public static final int firstname_emerg_add=0x7f040039;
        public static final int firstname_emerg_update=0x7f040046;
        public static final int firstname_update=0x7f040030;
        public static final int firstname_update_emerg=0x7f04003f;
        public static final int gender=0x7f040062;
        public static final int gender_add=0x7f040067;
        public static final int gender_update=0x7f040034;
        public static final int graphDisplay=0x7f04000a;
        public static final int header=0x7f040038;
        public static final int heartrate=0x7f04001d;
        public static final int height=0x7f04005e;
        public static final int height_add=0x7f040068;
        public static final int height_measure_add=0x7f040069;
        public static final int height_measure_update=0x7f040025;
        public static final int height_update=0x7f04002c;
        public static final int hybrid=0x7f040004;
        public static final int icon=0x7f040070;
        public static final int id=0x7f04005d;
        public static final int imageView1=0x7f040019;
        public static final int imageView2=0x7f04001a;
        public static final int label_username=0x7f040014;
        public static final int label_weight=0x7f040013;
        public static final int lastname=0x7f04005f;
        public static final int lastname_add=0x7f040064;
        public static final int lastname_add_emerg=0x7f040028;
        public static final int lastname_emerg_add=0x7f04003a;
        public static final int lastname_emerg_update=0x7f040047;
        public static final int lastname_update=0x7f040031;
        public static final int lastname_update_emerg=0x7f040040;
        public static final int layoutforboxes=0x7f040007;
        public static final int list=0x7f040012;
        public static final int locate_hospital=0x7f040075;
        public static final int location=0x7f04006f;
        public static final int login=0x7f040050;
        public static final int logout_menu=0x7f040076;
        public static final int mainlayoutforboxes=0x7f040017;
        public static final int mapView=0x7f040052;
        public static final int mobileno_add_emerg=0x7f04002b;
        public static final int mobileno_emerg_add=0x7f04003c;
        public static final int mobileno_emerg_update=0x7f040049;
        public static final int mobileno_update_emerg=0x7f040042;
        public static final int my_strip_tab=0x7f040072;
        public static final int name=0x7f04004d;
        public static final int none=0x7f040000;
        public static final int normal=0x7f040001;
        public static final int password=0x7f04004e;
        public static final int password_add=0x7f040066;
        public static final int password_update=0x7f040033;
        public static final int phone=0x7f04006e;
        public static final int phoneno_add_emerg=0x7f04002a;
        public static final int phoneno_emerg_add=0x7f04003b;
        public static final int phoneno_emerg_update=0x7f040048;
        public static final int phoneno_update_emerg=0x7f040041;
        public static final int posture=0x7f040021;
        public static final int pulse=0x7f04005a;
        public static final int recordList=0x7f040055;
        public static final int reference=0x7f04004c;
        public static final int register=0x7f040051;
        public static final int relation_add_emerg=0x7f04002f;
        public static final int relation_add_view=0x7f040037;
        public static final int relation_update_emerg=0x7f040045;
        public static final int relation_update_view=0x7f040044;
        public static final int satellite=0x7f040002;
        public static final int scrollView1=0x7f040016;
        public static final int scrollViewOfGroups=0x7f040006;
        public static final int separator=0x7f040015;
        public static final int separator2=0x7f040008;
        public static final int specialcomment=0x7f040022;
        public static final int switch1=0x7f040053;
        public static final int systolic=0x7f04001b;
        public static final int terrain=0x7f040003;
        public static final int time=0x7f040057;
        public static final int tip_of_the_day=0x7f040018;
        public static final int title=0x7f040071;
        public static final int unit=0x7f04005c;
        public static final int update_emergency=0x7f04004b;
        public static final int update_user=0x7f040036;
        public static final int updateemergcontact=0x7f04002e;
        public static final int user_update=0x7f040073;
        public static final int username=0x7f04004f;
        public static final int username_add=0x7f040065;
        public static final int username_update=0x7f040032;
        public static final int values=0x7f040059;
        public static final int weight=0x7f04005b;
        public static final int weight_add=0x7f04006a;
        public static final int weight_bpdata=0x7f04001e;
        public static final int weight_measure_add=0x7f04006b;
        public static final int weight_measure_update=0x7f040024;
        public static final int weight_measurement_bp=0x7f04001f;
        public static final int weight_update=0x7f040035;
        public static final int wipe_data=0x7f040054;
    }
    public static final class layout {
        public static final int activity_detailed=0x7f030000;
        public static final int activity_display=0x7f030001;
        public static final int activity_google=0x7f030002;
        public static final int activity_main=0x7f030003;
        public static final int addbp=0x7f030004;
        public static final int androidsettings=0x7f030005;
        public static final int editprofile=0x7f030006;
        public static final int emergency=0x7f030007;
        public static final int emergency_update=0x7f030008;
        public static final int footer_gradient=0x7f030009;
        public static final int groupbox_template=0x7f03000a;
        public static final int groupboxred_template=0x7f03000b;
        public static final int header_gradient=0x7f03000c;
        public static final int list_item=0x7f03000d;
        public static final int login=0x7f03000e;
        public static final int map_places=0x7f03000f;
        public static final int map_popup=0x7f030010;
        public static final int othersettings=0x7f030011;
        public static final int record_list_fragment=0x7f030012;
        public static final int record_listview=0x7f030013;
        public static final int record_listview_nodata=0x7f030014;
        public static final int register=0x7f030015;
        public static final int settingslist=0x7f030016;
        public static final int single_place=0x7f030017;
        public static final int tab_indicator=0x7f030018;
        public static final int userprofile_tablayout=0x7f030019;
    }
    public static final class menu {
        public static final int activity_main=0x7f090000;
        public static final int addbpmenu=0x7f090001;
        public static final int display=0x7f090002;
        public static final int edituserprofile=0x7f090003;
    }
    public static final class string {
        public static final int action_settings=0x7f050016;
        public static final int addButton=0x7f050037;
        public static final int addbpCancel=0x7f050036;
        public static final int addbpDiastolic=0x7f050031;
        public static final int addbpHeartRate=0x7f050032;
        public static final int addbpSpecialComments=0x7f050034;
        public static final int addbpSubmit=0x7f050035;
        public static final int addbpSystolic=0x7f050030;
        public static final int addbpWeight=0x7f050033;
        public static final int app_name=0x7f050011;
        /**   Title for notification shown when GooglePlayServices is unavailable [CHAR LIMIT=70] 
         */
        public static final int auth_client_availability_notification_title=0x7f05000e;
        /**   Title for notification shown when GooglePlayServices is unavailable [CHAR LIMIT=42] 
         */
        public static final int auth_client_play_services_err_notification_msg=0x7f05000f;
        /**   Requested by string saying which app requested the notification. [CHAR LIMIT=42] 
         */
        public static final int auth_client_requested_by_msg=0x7f050010;
        public static final int bpm=0x7f050042;
        /**  Button in confirmation dialog to enable Google Play services.  Clicking it
        will direct user to application settings of Google Play services where they
        can enable it [CHAR LIMIT=40] 
         */
        public static final int common_google_play_services_enable_button=0x7f050006;
        /**  Message in confirmation dialog informing user they need to enable
        Google Play services in application settings [CHAR LIMIT=NONE] 
         */
        public static final int common_google_play_services_enable_text=0x7f050005;
        /**  Title of confirmation dialog informing user they need to enable
        Google Play services in application settings [CHAR LIMIT=40] 
         */
        public static final int common_google_play_services_enable_title=0x7f050004;
        /**  Button in confirmation dialog for installing Google Play services [CHAR LIMIT=40] 
         */
        public static final int common_google_play_services_install_button=0x7f050003;
        /**  (For phones) Message in confirmation dialog informing user that
        they need to install Google Play services (from Play Store) [CHAR LIMIT=NONE] 
         */
        public static final int common_google_play_services_install_text_phone=0x7f050001;
        /**  (For tablets) Message in confirmation dialog informing user that
        they need to install Google Play services (from Play Store) [CHAR LIMIT=NONE] 
         */
        public static final int common_google_play_services_install_text_tablet=0x7f050002;
        /**  Title of confirmation dialog informing user that they need to install
        Google Play services (from Play Store) [CHAR LIMIT=40] 
         */
        public static final int common_google_play_services_install_title=0x7f050000;
        /**  Message in confirmation dialog informing user there is an unknown issue in Google Play
        services [CHAR LIMIT=NONE] 
         */
        public static final int common_google_play_services_unknown_issue=0x7f050009;
        /**  Message in confirmation dialog informing user that Google Play services is not supported on their device [CHAR LIMIT=NONE] 
         */
        public static final int common_google_play_services_unsupported_text=0x7f05000b;
        /**  Title of confirmation dialog informing user that Google Play services is not supported on their device [CHAR LIMIT=40] 
         */
        public static final int common_google_play_services_unsupported_title=0x7f05000a;
        /**  Button in confirmation dialog for updating Google Play services [CHAR LIMIT=40] 
         */
        public static final int common_google_play_services_update_button=0x7f05000c;
        /**  Message in confirmation dialog informing user that they need to update
        Google Play services (from Play Store) [CHAR LIMIT=NONE] 
         */
        public static final int common_google_play_services_update_text=0x7f050008;
        /**  Title of confirmation dialog informing user that they need to update
        Google Play services (from Play Store) [CHAR LIMIT=40] 
         */
        public static final int common_google_play_services_update_title=0x7f050007;
        /**  Long form sign-in button text. This is the placeholder text, used if we can't
        find the service-side assets. [CHAR LIMIT=25] 
         */
        public static final int common_signin_button_text_long=0x7f05000d;
        public static final int editAEC=0x7f05003b;
        public static final int emergencyAddress=0x7f05002c;
        public static final int emergencyCancel=0x7f05002f;
        public static final int emergencyEmail=0x7f05002b;
        public static final int emergencyFirst=0x7f050027;
        public static final int emergencyLast=0x7f050028;
        public static final int emergencyMobile=0x7f05002a;
        public static final int emergencyPhone=0x7f050029;
        public static final int emergencySubmit=0x7f05002e;
        public static final int emergencyText=0x7f05002d;
        public static final int gender=0x7f05003a;
        public static final int graph=0x7f050039;
        public static final int hello_world=0x7f050012;
        public static final int lbs=0x7f050044;
        public static final int loginButton=0x7f050019;
        public static final int loginPassword=0x7f05001b;
        public static final int loginUser=0x7f050018;
        public static final int logoDesc=0x7f050017;
        public static final int logout=0x7f05003c;
        public static final int menu_settings=0x7f050013;
        public static final int mmhg=0x7f050041;
        public static final int next=0x7f05003e;
        public static final int nodata=0x7f050043;
        public static final int placeholder=0x7f05003d;
        public static final int previous=0x7f05003f;
        public static final int registerAEC=0x7f050022;
        public static final int registerButton=0x7f05001a;
        public static final int registerCancel=0x7f050025;
        public static final int registerDOB=0x7f050026;
        public static final int registerFirst=0x7f05001c;
        public static final int registerHeight=0x7f050020;
        public static final int registerLast=0x7f05001d;
        public static final int registerPassword=0x7f05001f;
        public static final int registerSubmit=0x7f050023;
        public static final int registerUser=0x7f05001e;
        public static final int registerWeight=0x7f050021;
        public static final int settings=0x7f050038;
        public static final int text=0x7f050040;
        public static final int title_activity_display=0x7f050015;
        public static final int title_activity_main=0x7f050014;
        public static final int updateSubmit=0x7f050024;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080001;
        public static final int spacingForLL=0x7f080002;
    }
    public static final class styleable {
        /** Attributes that can be used with a MapAttrs.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #MapAttrs_cameraBearing cs.ecl.bp.app:cameraBearing}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_cameraTargetLat cs.ecl.bp.app:cameraTargetLat}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_cameraTargetLng cs.ecl.bp.app:cameraTargetLng}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_cameraTilt cs.ecl.bp.app:cameraTilt}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_cameraZoom cs.ecl.bp.app:cameraZoom}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_mapType cs.ecl.bp.app:mapType}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_uiCompass cs.ecl.bp.app:uiCompass}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_uiRotateGestures cs.ecl.bp.app:uiRotateGestures}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_uiScrollGestures cs.ecl.bp.app:uiScrollGestures}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_uiTiltGestures cs.ecl.bp.app:uiTiltGestures}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_uiZoomControls cs.ecl.bp.app:uiZoomControls}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_uiZoomGestures cs.ecl.bp.app:uiZoomGestures}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_useViewLifecycle cs.ecl.bp.app:useViewLifecycle}</code></td><td></td></tr>
           <tr><td><code>{@link #MapAttrs_zOrderOnTop cs.ecl.bp.app:zOrderOnTop}</code></td><td></td></tr>
           </table>
           @see #MapAttrs_cameraBearing
           @see #MapAttrs_cameraTargetLat
           @see #MapAttrs_cameraTargetLng
           @see #MapAttrs_cameraTilt
           @see #MapAttrs_cameraZoom
           @see #MapAttrs_mapType
           @see #MapAttrs_uiCompass
           @see #MapAttrs_uiRotateGestures
           @see #MapAttrs_uiScrollGestures
           @see #MapAttrs_uiTiltGestures
           @see #MapAttrs_uiZoomControls
           @see #MapAttrs_uiZoomGestures
           @see #MapAttrs_useViewLifecycle
           @see #MapAttrs_zOrderOnTop
         */
        public static final int[] MapAttrs = {
            0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003,
            0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007,
            0x7f010008, 0x7f010009, 0x7f01000a, 0x7f01000b,
            0x7f01000c, 0x7f01000d
        };
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#cameraBearing}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:cameraBearing
        */
        public static final int MapAttrs_cameraBearing = 1;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#cameraTargetLat}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:cameraTargetLat
        */
        public static final int MapAttrs_cameraTargetLat = 2;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#cameraTargetLng}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:cameraTargetLng
        */
        public static final int MapAttrs_cameraTargetLng = 3;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#cameraTilt}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:cameraTilt
        */
        public static final int MapAttrs_cameraTilt = 4;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#cameraZoom}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:cameraZoom
        */
        public static final int MapAttrs_cameraZoom = 5;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#mapType}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>none</code></td><td>0</td><td></td></tr>
<tr><td><code>normal</code></td><td>1</td><td></td></tr>
<tr><td><code>satellite</code></td><td>2</td><td></td></tr>
<tr><td><code>terrain</code></td><td>3</td><td></td></tr>
<tr><td><code>hybrid</code></td><td>4</td><td></td></tr>
</table>
          @attr name android:mapType
        */
        public static final int MapAttrs_mapType = 0;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#uiCompass}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:uiCompass
        */
        public static final int MapAttrs_uiCompass = 6;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#uiRotateGestures}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:uiRotateGestures
        */
        public static final int MapAttrs_uiRotateGestures = 7;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#uiScrollGestures}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:uiScrollGestures
        */
        public static final int MapAttrs_uiScrollGestures = 8;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#uiTiltGestures}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:uiTiltGestures
        */
        public static final int MapAttrs_uiTiltGestures = 9;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#uiZoomControls}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:uiZoomControls
        */
        public static final int MapAttrs_uiZoomControls = 10;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#uiZoomGestures}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:uiZoomGestures
        */
        public static final int MapAttrs_uiZoomGestures = 11;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#useViewLifecycle}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:useViewLifecycle
        */
        public static final int MapAttrs_useViewLifecycle = 12;
        /**
          <p>This symbol is the offset where the {@link cs.ecl.bp.app.R.attr#zOrderOnTop}
          attribute's value can be found in the {@link #MapAttrs} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:zOrderOnTop
        */
        public static final int MapAttrs_zOrderOnTop = 13;
    };
}
