package cs.ecl.bp.app;

import java.util.Calendar;
import java.util.HashMap;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import cs.ecl.bp.app.contentprovider.BPContentProvider;
import cs.ecl.bp.app.database.BPUserDataTable;
import cs.ecl.bp.app.datastorage.BPUserData;

public class AddBPActivity extends Activity {

	String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
	Uri userDataTable = Uri.parse(AUTH + BPUserDataTable.TABLE_NAME);

	SessionManagement session;
	EditText systolic;
	EditText diastolic;
	EditText heartRate;
	EditText weight;
	EditText specialcomments;
	Spinner posture;
	Spinner bodypart;
	Spinner weight_measurements;
	String posture_type;
	String bodypart_type;
	String weight_type;
	Button submit;
	Button cancel;
	public static int id;

	private static final String TAG = "registeract";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addbp);

		session = new SessionManagement(getApplicationContext());
		session.checkLogin();

		ActionBar bar1 = getActionBar();
		bar1.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#132C87")));
		bar1.setDisplayHomeAsUpEnabled(true);

		// Get session details
		HashMap<String, String> user = session.getUserDetails();
		// String name = user.get(session.KEY_NAME);
		id = Integer.parseInt(user.get(SessionManagement.KEY_ID));

		submit = (Button) findViewById(R.id.add_bpdata);
		systolic = (EditText) findViewById(R.id.systolic);
		diastolic = (EditText) findViewById(R.id.diastolic);
		heartRate = (EditText) findViewById(R.id.heartrate);
		specialcomments = (EditText) findViewById(R.id.specialcomment);
		weight = (EditText) findViewById(R.id.weight_bpdata);
		posture = (Spinner) findViewById(R.id.posture);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.posture_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		posture.setAdapter(adapter);
		posture.setOnItemSelectedListener(new postureOnItemSelectedListener());
		bodypart = (Spinner) findViewById(R.id.bodypart);
		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(
				this, R.array.bodyPart_array,
				android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		bodypart.setAdapter(adapter2);
		bodypart.setOnItemSelectedListener(new bodypartOnItemSelectedListener());
		weight_measurements = (Spinner) findViewById(R.id.weight_measurement_bp);
		ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(
				this, R.array.weight_array,
				android.R.layout.simple_spinner_item);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_item);
		weight_measurements.setAdapter(adapter3);
		weight_measurements
				.setOnItemSelectedListener(new weightOnItemSelectedListener());

		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				String systolic_ = systolic.getText().toString();
				Log.w(TAG, "Systolic :" + systolic);
				String diastolic_ = diastolic.getText().toString();
				Log.w(TAG, "Diastolic :" + diastolic);
				String heartRate_ = heartRate.getText().toString();
				Log.w(TAG, "Heart Rate :" + heartRate_);
				String weight_;
				if (weight_type.matches("kg")) {
					weight_ = weight.getText().toString() + "kg";
				} else {
					weight_ = weight.getText().toString() + "lbs";
				}
				Log.w(TAG, "Weight :" + weight_);
				String posture_ = posture_type;
				Log.w(TAG, "Posturer :" + posture_type);
				String bodypart_ = bodypart_type;
				Log.w(TAG, "Body Part :" + bodypart_);
				String comments_ = specialcomments.getText().toString();
				Log.w(TAG, "Comment :" + comments_);

				Calendar today = Calendar.getInstance();
				int mYear = today.get(Calendar.YEAR);
				int mMonth = today.get(Calendar.MONTH);
				int mDay = today.get(Calendar.DAY_OF_MONTH);
				String date_entered = Integer.toString(mYear) + "/"
						+ Integer.toString(mMonth) + "/"
						+ Integer.toString(mDay);
				if (systolic_.isEmpty() || diastolic_.isEmpty()
						|| heartRate_.isEmpty() || weight_.isEmpty()
						|| posture_.isEmpty() || bodypart_.isEmpty()
						|| comments_.isEmpty()) {
					Toast.makeText(getApplicationContext(),
							"Please enter all required data", Toast.LENGTH_LONG)
							.show();
				} else {
					BPUserData data = new BPUserData(systolic_, diastolic_,
							heartRate_, weight_, bodypart_, posture_,
							comments_, "120", date_entered);
					insertUserData(data);
					UserDataInstance x = new UserDataInstance(date_entered, heartRate_, systolic_, diastolic_, "0", weight_);
					Toast.makeText(getApplicationContext(),
							"Blood pressure data added", Toast.LENGTH_LONG)
							.show();
					Intent i = new Intent(getApplicationContext(),
							MainActivity.class);
					i.putExtra("systolic", x.recommendation(1));
					i.putExtra("diastolic", x.recommendation(2));
					startActivity(i);
					finish();
				}

			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.addbpmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent h = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(h);
			finish();
			break;
		case R.id.logout_menu:
			session.logoutUser();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public class postureOnItemSelectedListener implements
			OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			posture_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	public class bodypartOnItemSelectedListener implements
			OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			bodypart_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	public class weightOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			weight_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	private void insertUserData(BPUserData data) {
		ContentValues cv = new ContentValues();
		cv.put(BPUserDataTable.COLUMN_SYSTOLIC, data.getSystolic());
		cv.put(BPUserDataTable.COLUMN_DIASTOLIC, data.getDiastolic());
		cv.put(BPUserDataTable.COLUMN_HEARTRATE, data.getHeartRate());
		cv.put(BPUserDataTable.COLUMN_WEIGHT, data.getWeight());
		cv.put(BPUserDataTable.COLUMN_BODYPART, data.getBodyPart());
		cv.put(BPUserDataTable.COLUMN_POSTURE, data.getPosture());
		cv.put(BPUserDataTable.COLUMN_BLOODPRESSURE, "0");
		cv.put(BPUserDataTable.COLUMN_COMMENT, data.getBPComment());
		cv.put(BPUserDataTable.COLUMN_RECOMMENDATION, "Light Exersize");
		cv.put(BPUserDataTable.COLUMN_DATEENTERED, data.getDateEntered());
		cv.put(BPUserDataTable.COLUMN_USERID, id);
		getContentResolver().insert(userDataTable, cv);
		cv.clear();
	}
}
