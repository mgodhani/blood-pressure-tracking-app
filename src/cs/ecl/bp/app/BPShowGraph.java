package cs.ecl.bp.app;

import java.util.ArrayList;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

public class BPShowGraph{
	
	
	
	int counter =0;
	int total;
	
	ArrayList<Integer> systolicArray, diastolicArray, pulseArray;

	public Intent getIntent(Context context,Intent intent) {
		// Our first data
		
		Bundle b = intent.getExtras();
		systolicArray = b.getIntegerArrayList("systolic");
		diastolicArray = b.getIntegerArrayList("diastolic");
		pulseArray = b.getIntegerArrayList("pulse");
		
		//get the amount of points
		total = systolicArray.size();
		
		Log.i("systolicArray Length ",systolicArray.size()+"");
		Log.i("diastolicArray Length ",diastolicArray.size()+"");
		Log.i("pulseArray Length ",pulseArray.size()+"");
		
		TimeSeries series = new TimeSeries("Systolic"); 
		for( int i = 0; i < total; i++)
		{
			series.add(i, systolicArray.get(i));
		}
		
		// Our second data
		TimeSeries series2 = new TimeSeries("Diastolic"); 
		for( int i = 0; i < total ; i++)
		{
			series2.add(i, diastolicArray.get(i));
		}
		
		// Our third data
		TimeSeries series3 = new TimeSeries("Pulse"); 
		for( int i = 0; i < total; i++)
		{
			series3.add(i, pulseArray.get(i));
		}
		
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		dataset.addSeries(series);
		dataset.addSeries(series2);
		dataset.addSeries(series3);
		
		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer(); // Holds a collection of XYSeriesRenderer and customizes the graph
		XYSeriesRenderer renderer = new XYSeriesRenderer(); // This will be used to customize line 1
		XYSeriesRenderer renderer2 = new XYSeriesRenderer(); // This will be used to customize line 2
		XYSeriesRenderer renderer3 = new XYSeriesRenderer(); // This will be used to customize line 2
		mRenderer.setChartTitle("Summary");
		mRenderer.setYTitle("BPTypes");
		mRenderer.setXTitle("Times");
		mRenderer.addSeriesRenderer(renderer);
		mRenderer.addSeriesRenderer(renderer2);
		mRenderer.addSeriesRenderer(renderer3);
		
		// Customization time for line 1!
		renderer.setColor(Color.RED);
		renderer.setPointStyle(PointStyle.SQUARE);
		renderer.setFillPoints(true);
		// Customization time for line 2!
		renderer2.setColor(Color.BLUE);
		renderer2.setPointStyle(PointStyle.DIAMOND);
		renderer2.setFillPoints(true);
		
		renderer3.setColor(Color.GREEN);
		renderer3.setPointStyle(PointStyle.DIAMOND);
		renderer3.setFillPoints(true);
	
		
		Intent intnt = ChartFactory.getLineChartIntent(context, dataset, mRenderer);
		return intnt;
		
	}

}