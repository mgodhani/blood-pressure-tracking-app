package cs.ecl.bp.app;

import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class BPTabLayout extends TabActivity {
	
	SessionManagement session;
	TabHost tabHost;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.userprofile_tablayout);
	        
	        session = new SessionManagement(getApplicationContext());
			session.checkLogin();
			
			tabHost = getTabHost();
	
	    	ActionBar bar = getActionBar();
	    	
	    	bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#132C87")));
			bar.setDisplayHomeAsUpEnabled(true);
	        
	        
	     // Adding all TabSpec to TabHost
	        addTab("Profile",R.drawable.icon_profile_tab); // Adding photos tab
	        addTab("Emergency Contact",R.drawable.icon_contact_tab); // Adding songs tab
	        addTab("Other Settings", R.drawable.icon_settings_tab);
	     
	 }
	 
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.edituserprofile, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {
			case android.R.id.home:
				Intent h = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(h);
				finish();
				break;
			case R.id.user_update:
				Intent i = new Intent(getApplicationContext(),
						UpdateUserProfileActivity.class);
				startActivity(i);
				finish();
				break;
			case R.id.logout_menu:
				session.logoutUser();
				break;
			}
			return super.onOptionsItemSelected(item);
		}
		
		private void addTab(String label, int drawableId){
			if(label.matches("Profile")){
				Intent intent = new Intent(this,UpdateUserProfileActivity.class);
				TabSpec spec = tabHost.newTabSpec(label);
				View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(),false);
				TextView title = (TextView) tabIndicator.findViewById(R.id.title);
				title.setText(label);
				ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
				icon.setImageResource(drawableId);
				spec.setIndicator(tabIndicator);
				spec.setContent(intent);
				tabHost.addTab(spec);
				
			}
			if(label.matches("Emergency Contact")){
				Intent intent = new Intent(this,UpdateEmergencyContact.class);
				TabHost.TabSpec spec = tabHost.newTabSpec(label);
				View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(),false);
				TextView title = (TextView) tabIndicator.findViewById(R.id.title);
				title.setText(label);
				ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
				icon.setImageResource(drawableId);
				spec.setIndicator(tabIndicator);
				spec.setContent(intent);
				tabHost.addTab(spec);
				
			}
			if(label.matches("Other Settings")){
				Intent intent = new Intent(this,OtherSettingsActivity.class);
				TabHost.TabSpec spec = tabHost.newTabSpec(label);
				View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(),false);
				TextView title = (TextView) tabIndicator.findViewById(R.id.title);
				title.setText(label);
				ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
				icon.setImageResource(drawableId);
				spec.setIndicator(tabIndicator);
				spec.setContent(intent);
				tabHost.addTab(spec);
				
			}
		}

}
