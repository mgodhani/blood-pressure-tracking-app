package cs.ecl.bp.app;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidplot.Plot;
import com.androidplot.series.XYSeries;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepFormatter;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYStepMode;
import cs.ecl.bp.app.contentprovider.BPContentProvider;
import cs.ecl.bp.app.database.BPUserDataTable;

@SuppressLint("SimpleDateFormat")
public class DetailedActivity extends FragmentActivity {

	SessionManagement session;
	int rangeToDisplay = 1;
	// 1=Daily
	// 2=Weekly
	// 3=Monthly
	// 4=Past Month
	// 5=Yearly
	int graphToDisplay = 1;
	// 1=Diastolic BP
	// 2=Systolic BP
	// 3=Weight
	// 4=Heart Rate
	private XYPlot mySimpleXYPlot, mSimpleXYBar;
	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");	
	List<UserDataInstance> udi = new ArrayList<UserDataInstance>();
	
	//CP stuff
	String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
	Uri userDataTable = Uri.parse(AUTH + BPUserDataTable.TABLE_NAME);
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressWarnings("static-access")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detailed);
		
		session = new SessionManagement(getApplicationContext());
		session.checkLogin();
		
		ActionBar bar = getActionBar();
		
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#132C87")));
		bar.setDisplayHomeAsUpEnabled(true);

		// Get session details
		HashMap<String, String> user = session.getUserDetails();
		//String name = user.get(session.KEY_NAME);
		String id = user.get(session.KEY_ID);
		
		// Get range from intent
		Intent intent = getIntent();
		rangeToDisplay = intent.getIntExtra("range", 0);
		
		//get data from db
		List<UserDataInstance> temp = new ArrayList<UserDataInstance>();
		Cursor getUserData = getContentResolver().query(
				userDataTable,
				null,
				BPUserDataTable.COLUMN_USERID + "=" + "'" + id
						+ "'", null, null);
		
		if (getUserData.moveToFirst()) {
			do {
				System.out.println("Row added");
				temp.add(new UserDataInstance(getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_DATEENTERED)),
											 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_HEARTRATE)),
											 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_SYSTOLIC)),
											 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_DIASTOLIC)),
											 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_BLOODPRESSURE)),
											 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_WEIGHT))));
			} while (getUserData.moveToNext());
		}
			
		
		//Filter stuff not in range
		Calendar cal = GregorianCalendar.getInstance();
		Date d = new Date();
		
		switch(rangeToDisplay){
		case 1: 
			//range = "Today";
			//if scrollview is available
			cal.setTime(d);
			cal.add(GregorianCalendar.DATE, -1);
			
			if (findViewById(R.id.scrollViewOfGroups) != null) {
				TextView tv = (TextView) findViewById(R.id.dateRange);
				tv.setText("Today (" + dateFormat.format(d.getTime()) + ")");
			}
			for (int i = 0; i < temp.size(); i++){
				if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
					udi.add(temp.get(i));
				}
			}
			break;
		case 2:
			//range = "This Week";
			cal.setTime(d);
			cal.add(GregorianCalendar.DATE, -7);
			
			if (findViewById(R.id.scrollViewOfGroups) != null) {
				TextView tv = (TextView) findViewById(R.id.dateRange);
				tv.setText("This Week (" + dateFormat.format(cal.getTime()) + " - " + dateFormat.format(d) + ")");
			}
			for (int i = 0; i < temp.size(); i++){
				if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
					udi.add(temp.get(i));
				}
			}
			break;
		case 3:
			//range = "This Month";
			cal.setTime(d);
			cal.add(GregorianCalendar.MONTH, -1);
			
			if (findViewById(R.id.scrollViewOfGroups) != null) {
				TextView tv = (TextView) findViewById(R.id.dateRange);
				tv.setText("This Month (" + dateFormat.format(cal.getTime()) + " - " + dateFormat.format(d) + ")");
			}
			
			for (int i = 0; i < temp.size(); i++){
				if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
					udi.add(temp.get(i));
				}
			}
			break;
		case 4:
			//range = "Last Month";
			cal.setTime(d);
			cal.add(GregorianCalendar.MONTH, -2);
			
			Calendar cal2 = GregorianCalendar.getInstance();
			cal2.setTime(d);
			cal2.add(GregorianCalendar.MONTH, -1);
			
			if (findViewById(R.id.scrollViewOfGroups) != null) {
				TextView tv = (TextView) findViewById(R.id.dateRange);
				tv.setText("Last Month (" + dateFormat.format(cal.getTime()) + " - " + dateFormat.format(cal2.getTime()) + ")");
			}
			
			for (int i = 0; i < temp.size(); i++){
				if (isWithinRange(temp.get(i).get_date(), cal.getTime(), cal2.getTime())){
					udi.add(temp.get(i));
				}
			}
			break;
		case 5:
			//range = "Last Year";
			cal.setTime(d);
			cal.add(GregorianCalendar.YEAR, -1);
			
			if (findViewById(R.id.scrollViewOfGroups) != null) {
				TextView tv = (TextView) findViewById(R.id.dateRange);
				tv.setText("Last Year (" + dateFormat.format(cal.getTime()) + " - " + dateFormat.format(d.getTime()) + ")");
			}
			
			for (int i = 0; i < temp.size(); i++){
				if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
					udi.add(temp.get(i));
				}
			}
			break;
		default:
			//unknown
			break;
	}
		
		
		//Sort the list of UDI by date
		Collections.sort(udi, new UDICustomComparator());
		
		if (findViewById(R.id.graphDisplay) != null) {
			buildGraphView();
		}
		if (findViewById(R.id.scrollViewOfGroups) != null) {
			buildDetailedView();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void onClick(View view) {	
		switch(view.getId()) {
		case R.id.buttonPrevious:
			graphToDisplay--;
			if (graphToDisplay < 1){
				graphToDisplay = 5;
			}
			buildGraphView();
			break;
		case R.id.buttonNext:
			graphToDisplay++;
			if (graphToDisplay > 5){
				graphToDisplay = 1;
			}
			buildGraphView();
			break;
		case R.id.btnAnalysis:
			ArrayList<Integer> systolicArray, diastolicArray, pulseArray;
			
			systolicArray = new ArrayList<Integer>();
			systolicArray.add(105);
			systolicArray.add(95);
			systolicArray.add(100);
			
			diastolicArray = new ArrayList<Integer>();
			diastolicArray.add(70);
			diastolicArray.add(55);
			diastolicArray.add(80);
			
			pulseArray = new ArrayList<Integer>();
			pulseArray.add(92);
			pulseArray.add(90);
			pulseArray.add(94);
			
			Intent intent = new Intent();
			intent.putIntegerArrayListExtra("systolic", systolicArray);
			intent.putIntegerArrayListExtra("diastolic", diastolicArray);
			intent.putIntegerArrayListExtra("pulse", pulseArray);
			
			BPShowGraph line = new BPShowGraph();
			Intent lineIntent = line.getIntent(this, intent);
			lineIntent.setClass(this, GraphLayout.class);
			startActivity(lineIntent);
			break;
		}
	}
	
	boolean bar = false;
	
	@SuppressWarnings({ "deprecation", "serial" })
	@SuppressLint("SimpleDateFormat")
	public void buildGraphView(){
		System.out.println("Rebuild Graph");
		
		String title = "Graph";
		String range = "Range";
		String measurement = "mmHg";
		Calendar cal = GregorianCalendar.getInstance();
		Date d = new Date();	//Date:     Today
		cal.setTime(d);			//Calendar: Today
		List<Number> values = new ArrayList<Number>();
		List<Number> dates = new ArrayList<Number>();
		
		switch(rangeToDisplay){
			case 1: 
				//range = "Today";
				range = "Today, " + dateFormat.format(d);
				//daily
				break;
			case 2:
				cal.setTime(d);
				cal.add(GregorianCalendar.DATE, -7);
				//range = "This Week";
				range = "This week, " + dateFormat.format(cal.getTime()) + "-" + dateFormat.format(d);
				//weekly
				break;
			case 3:
				cal.setTime(d);
				cal.add(GregorianCalendar.MONTH, -1);
				//range = "This Month";
				range = "This Month, " + dateFormat.format(cal.getTime()) + "-" + dateFormat.format(d);
				//current month
				break;
			case 4:
				cal.setTime(d);
				cal.add(GregorianCalendar.MONTH, -1);
				//range = "Last Month";
				range = "Last Month, " + dateFormat.format(cal.getTime()) + "-" + dateFormat.format(d);
				//previous month
				break;
			case 5:
				cal.setTime(d);
				cal.add(GregorianCalendar.YEAR, -1);
				//range = "Last Year";
				range = "Last Year, " + dateFormat.format(cal.getTime()) + "-" + dateFormat.format(d);
				//yearly
				break;
			default:
				//unknown
				break;
		}
		
		switch (this.graphToDisplay){
			case 1:
				bar = false;
				// 2=Systolic BP
				title = "Systolic Blood Pressure";
				measurement = "mmHg";
				for (int i = 0; i<udi.size(); i++){
					values.add(udi.get(i).getSystolic());
					dates.add(udi.get(i).get_date().getTime());
				}
				break;
			case 2:
				bar = false;
				// 1=Diastolic BP	
				title = "Diastolic Blood Pressure";
				measurement = "mmHg";
				for (int i = 0; i<udi.size(); i++){
					values.add(udi.get(i).getDiastolic());
					dates.add(udi.get(i).get_date().getTime());
				}
				break;
			case 3:
				bar = false;
				// 3=Weight
				title = "Weight";
				measurement = "lbs";
				for (int i = 0; i<udi.size(); i++){
					values.add(udi.get(i).getValueOfWeight());
					dates.add(udi.get(i).get_date().getTime());
				}
				break;
			case 4:
				bar = false;
				// 4=Heart Rate
				title = "Heart Rate";
				measurement = "BPM";
				for (int i = 0; i<udi.size(); i++){
					//System.out.println("NUMBER:'" + udi.get(i).getHeartrate() + "'");
					values.add(udi.get(i).getHeartrate());
					dates.add(udi.get(i).get_date().getTime());
				}
				break;
			case 5:
				bar = true;
				break;
			default:
				break;
		}
		
		// initialize XYPlot:
        mySimpleXYPlot = (XYPlot) findViewById(R.id.graphDisplay);
        mSimpleXYBar = (XYPlot) findViewById(R.id.barDisplay);
        mSimpleXYBar.setVisibility(XYPlot.INVISIBLE);
		
        mySimpleXYPlot.clear();
        mSimpleXYBar.clear();
        
        if (bar) {
        	mySimpleXYPlot.setVisibility(XYPlot.INVISIBLE);
        	mSimpleXYBar.setVisibility(XYPlot.VISIBLE);
        	
            mSimpleXYBar.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);
            mSimpleXYBar.getTitleWidget().getLabelPaint().setTextSize(30);
            mSimpleXYBar.getTitleWidget().setSize(new SizeMetrics(50, SizeLayoutType.ABSOLUTE, 1000, SizeLayoutType.ABSOLUTE));
            mSimpleXYBar.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);
            mSimpleXYBar.getGraphWidget().setGridPaddingBottom(40);
            mSimpleXYBar.getGraphWidget().setGridPaddingTop(40);
            mSimpleXYBar.getGraphWidget().setGridPaddingLeft(40);
            mSimpleXYBar.getGraphWidget().setGridPaddingRight(40);
            mSimpleXYBar.getGraphWidget().setRangeLabelWidth(25);
     
            mSimpleXYBar.setBorderStyle(Plot.BorderStyle.SQUARE, null, null);
            mSimpleXYBar.getBorderPaint().setStrokeWidth(1);
            mSimpleXYBar.getBorderPaint().setAntiAlias(false);
            mSimpleXYBar.getBorderPaint().setColor(Color.WHITE);
     
            // setup our line fill paint to be a slightly transparent gradient:
            Paint lineFill = new Paint();
            lineFill.setAlpha(200);
            lineFill.setShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.BLUE, Shader.TileMode.MIRROR));
     
            StepFormatter stepFormatter  = new StepFormatter(Color.rgb(0, 0,0), Color.BLUE);
            stepFormatter.getLinePaint().setStrokeWidth(1);
            
         // y-vals to plot:
            Number[] series1Numbers = {1, 2, 3, 4, 2, 3, 4, 2, 2, 2, 3, 4, 2, 3, 2, 2};
     
            // create our series from our array of nums:
            SimpleXYSeries series2 = new SimpleXYSeries(
                    Arrays.asList(series1Numbers),
                    SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                    "Heart Rate");
     
            stepFormatter.getLinePaint().setAntiAlias(false);
            stepFormatter.setFillPaint(lineFill);
            mSimpleXYBar.addSeries(series2, stepFormatter);
     
            // adjust the domain/range ticks to make more sense; label per tick for range and label per 5 ticks domain:
            mSimpleXYBar.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1);
            mSimpleXYBar.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1);
            mSimpleXYBar.setTicksPerRangeLabel(1);
            mSimpleXYBar.setTicksPerDomainLabel(5);
     
            // customize our domain/range labels
            mSimpleXYBar.setDomainLabel("Time (Secs)");
            mSimpleXYBar.setRangeLabel("Server State");
            
         // customize our domain/range labels
            mSimpleXYBar.getDomainLabelWidget().setSize(new SizeMetrics(50, SizeLayoutType.ABSOLUTE, 1000, SizeLayoutType.ABSOLUTE));
            mSimpleXYBar.getDomainLabelWidget().getLabelPaint().setTextSize(30);
            mSimpleXYBar.getGraphWidget().getDomainLabelPaint().setTextSize(20);
            
            mSimpleXYBar.getRangeLabelWidget().setSize(new SizeMetrics(500, SizeLayoutType.ABSOLUTE, 50, SizeLayoutType.ABSOLUTE));
            mSimpleXYBar.getRangeLabelWidget().getLabelPaint().setTextSize(30);
            mSimpleXYBar.getGraphWidget().getRangeLabelPaint().setTextSize(20);
     
            // get rid of decimal points in our domain labels:
            mSimpleXYBar.setDomainValueFormat(new DecimalFormat("0"));
     
            // create a custom formatter to draw our state names as range tick labels:
            mSimpleXYBar.setRangeValueFormat(new Format() {
                @Override
                public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                    Number num = (Number) obj;
                    switch(num.intValue()) {
                        case 1:
                            toAppendTo.append("Low");
                            break;
                        case 2:
                            toAppendTo.append("Medium");
                            break;
                        case 3:
                            toAppendTo.append("Average");
                            break;
                        case 4:
                            toAppendTo.append("High");
                            break;
                        default:
                            toAppendTo.append("Unknown");
                            break;
                    }
                    return toAppendTo;
                }
     
                @Override
                public Object parseObject(String source, ParsePosition pos) {
                    return null;
                }
            });
            mSimpleXYBar.redraw();
            //mySimpleXYPlot.removeSeries(series2);
        } else {
        	mySimpleXYPlot.setVisibility(XYPlot.VISIBLE);
        	mSimpleXYBar.setVisibility(XYPlot.INVISIBLE);
        	
            mySimpleXYPlot.setTitle(title + " (" + range + ")");
            mySimpleXYPlot.getTitleWidget().getLabelPaint().setTextSize(30);
            mySimpleXYPlot.getTitleWidget().setSize(new SizeMetrics(50, SizeLayoutType.ABSOLUTE, 1000, SizeLayoutType.ABSOLUTE));
            mySimpleXYPlot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);
            mySimpleXYPlot.getGraphWidget().setMargins(40, 40, 40, 40);
            mySimpleXYPlot.getGraphWidget().setRangeLabelWidth(25); 
            
            mySimpleXYPlot.setBorderStyle(Plot.BorderStyle.SQUARE, null, null);
            mySimpleXYPlot.getBorderPaint().setStrokeWidth(1);
            mySimpleXYPlot.getBorderPaint().setAntiAlias(false);
            mySimpleXYPlot.getBorderPaint().setColor(Color.WHITE);
        	
        	// setup our line fill paint to be a slightly transparent gradient:
            Paint lineFill = new Paint();
            lineFill.setAlpha(200);
            lineFill.setShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.BLUE, Shader.TileMode.MIRROR));
        	
        	// Create a formatter to use for drawing a series using LineAndPointRenderer:
            LineAndPointFormatter formatter = new LineAndPointFormatter(
                    Color.rgb(0, 200, 0),                   // line color
                    Color.rgb(0, 100, 0),                   // point color
                    Color.rgb(100, 200, 0));                                  // fill color (none)
            formatter.setFillPaint(lineFill);
            
            XYSeries seriesToDisplay = new SimpleXYSeries(dates, values, title);
            
            mySimpleXYPlot.addSeries(seriesToDisplay, formatter);
            
         // draw a domain tick for each year:
            mySimpleXYPlot.setDomainStep(XYStepMode.SUBDIVIDE, dates.size());

            // customize our domain/range labels
            mySimpleXYPlot.setDomainLabel("Date");
            mySimpleXYPlot.getDomainLabelWidget().setSize(new SizeMetrics(50, SizeLayoutType.ABSOLUTE, 1000, SizeLayoutType.ABSOLUTE));
            mySimpleXYPlot.getDomainLabelWidget().getLabelPaint().setTextSize(30);
            mySimpleXYPlot.getGraphWidget().getDomainLabelPaint().setTextSize(20);
            
            mySimpleXYPlot.setRangeLabel(measurement);
            mySimpleXYPlot.getRangeLabelWidget().setSize(new SizeMetrics(500, SizeLayoutType.ABSOLUTE, 50, SizeLayoutType.ABSOLUTE));
            mySimpleXYPlot.getRangeLabelWidget().getLabelPaint().setTextSize(30);
            mySimpleXYPlot.getGraphWidget().getRangeLabelPaint().setTextSize(20);
            
            // get rid of decimal points in our range labels:
            mySimpleXYPlot.setRangeValueFormat(new DecimalFormat("0"));

    	    mySimpleXYPlot.setDomainValueFormat(new Format() {
    	        private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");;
    	
    	        @Override
    	        public StringBuffer format(Object arg0, StringBuffer arg1,FieldPosition arg2) {
    	            long timestamp = ((Number) arg0).longValue();
    	            Date date = new Date(timestamp);
    	            return dateFormat.format(date, arg1, arg2);
    	        }
    	
    	        @Override
    	        public Object parseObject(String source, ParsePosition pos) {
    	            return null;
    	        }
    	    });
            
            // reduce the number of range labels
            mySimpleXYPlot.setTicksPerRangeLabel(1);
            mySimpleXYPlot.redraw();
        }

        mySimpleXYPlot.disableAllMarkup();
        mSimpleXYBar.disableAllMarkup();
        
	}
	
	public void buildDetailedView(){
		LinearLayout ll = (LinearLayout) findViewById(R.id.layoutforboxes);
		
		for (int i = 0; i<udi.size(); i++){
			//TextView tv;
			boolean systolicFlag = false;
			boolean diastolicFlag = false;
			
			if (udi.get(i).recommendation(1)!=2){
				systolicFlag = true;
			}
			if (udi.get(i).recommendation(2)!=2){
				diastolicFlag = true;
			}
			
			LinearLayout rowOfData = (LinearLayout)getLayoutInflater().inflate(R.layout.record_listview, null);
			TextView tv_title = (TextView) rowOfData.findViewById( R.id.time );
			tv_title.setText(udi.get(i).getStringDate());
			TextView tv_bp = (TextView) rowOfData.findViewById( R.id.values );
			if (diastolicFlag || systolicFlag){
				tv_bp.setTextColor(Color.RED);
			}
			tv_bp.setText(udi.get(i).getSystolic() + "/" + udi.get(i).getDiastolic());
			TextView tv_weight = (TextView) rowOfData.findViewById( R.id.weight );
			tv_weight.setText(udi.get(i).getWeight() + "");
			TextView tv_heartrate = (TextView) rowOfData.findViewById( R.id.pulse );
			tv_heartrate.setText(udi.get(i).getHeartrate() + "");
			
			rowOfData.setId(i);
			rowOfData.setPadding(10,10,10,10);
			ll.addView(rowOfData); //,lp
		}
	}
	
	boolean isWithinRange(Date testDate, Date startDate, Date endDate) {
		return !(testDate.before(startDate) || testDate.after(endDate));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent h = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(h);
			finish();
			break;
		case R.id.user_update:
			Intent i = new Intent(getApplicationContext(),
					UpdateUserProfileActivity.class);
			startActivity(i);
			finish();
			break;
		case R.id.add_data:
			Intent intent = new Intent(getApplicationContext(),AddBPActivity.class);
			startActivity(intent);
			break;
		case R.id.logout_menu:
			session.logoutUser();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
