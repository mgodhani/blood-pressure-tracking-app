package cs.ecl.bp.app;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class DisplayActivity extends Activity {
 
	TableLayout tlQ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display);
		tlQ = (TableLayout) findViewById(R.id.TableLayout1);
		
		Bundle d = getIntent().getExtras();
		ArrayList<String> data = new ArrayList<String>();
		TableRow tr;
		TextView tv;
		
		if (d != null){
			data = (ArrayList<String>) d.getStringArrayList("data");
			for (String item : data) {
				tr = new TableRow(this);
				tv = new TextView(this);
				tv.setText(item);
				tv.setTextSize(20);
				tr.addView(tv);
				tlQ.addView(tr, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display, menu);
		return true;
	}

}
