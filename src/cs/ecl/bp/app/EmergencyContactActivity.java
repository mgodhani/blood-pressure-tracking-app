package cs.ecl.bp.app;

import java.util.Calendar;

import cs.ecl.bp.app.contentprovider.BPContentProvider;
import cs.ecl.bp.app.database.BPContactInformationTable;
import cs.ecl.bp.app.database.BPUserTable;
import cs.ecl.bp.app.datastorage.BPContactInfo;
import cs.ecl.bp.app.datastorage.BPUserProfile;
import cs.ecl.bp.app.RegisterActivity;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class EmergencyContactActivity extends Activity {

	String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
	Uri userTable = Uri.parse(AUTH + BPUserTable.TABLE_NAME);
	Uri contactInformation = Uri.parse(AUTH
			+ BPContactInformationTable.TABLE_NAME);

	Uri userUri;
	Button submit;
	EditText firstname_emerg;
	EditText lastname_emerg;
	EditText phoneno_emerg;
	EditText mobileno_emerg;
	EditText address_emerg;
	Spinner Relation;
	RegisterActivity registeract;
	BPUserProfile adduser = RegisterActivity.insert_user;
	String relation_type;
	BPContactInfo contact;
	public static Integer add_id;
	public static Boolean user_added = false;
	public static Boolean contact_added = false;

	private static final String TAG = "emergencyact";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.emergency);

		submit = (Button) findViewById(R.id.add_emergency);

		firstname_emerg = (EditText) findViewById(R.id.firstname_emerg_add);
		lastname_emerg = (EditText) findViewById(R.id.lastname_emerg_add);
		phoneno_emerg = (EditText) findViewById(R.id.phoneno_emerg_add);
		mobileno_emerg = (EditText) findViewById(R.id.mobileno_emerg_add);
		address_emerg = (EditText) findViewById(R.id.address_emerg_add);

		Relation = (Spinner) findViewById(R.id.relation_add_emerg);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.relation_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Relation.setAdapter(adapter);
		Relation.setOnItemSelectedListener(new myOnItemSelectedListener());

		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String first_name = firstname_emerg.getText().toString();
				Log.w(TAG, "First Name :" + first_name);
				String last_name = lastname_emerg.getText().toString();
				Log.w(TAG, "Last Name :" + last_name);
				String phone_no = phoneno_emerg.getText().toString();
				Log.w(TAG, "Phone No :" + phone_no);
				String mobilephone_no = mobileno_emerg.getText().toString();
				Log.w(TAG, "Mobile Phone :" + mobilephone_no);
				String address = address_emerg.getText().toString();
				Log.w(TAG, "Address :" + address);
				String relation_ = relation_type;
				Log.w(TAG, "Relation :" + relation_);
				Calendar today = Calendar.getInstance();
				int mYear = today.get(Calendar.YEAR);
				int mMonth = today.get(Calendar.MONTH);
				int mDay = today.get(Calendar.DAY_OF_MONTH);
				String date_entered = Integer.toString(mYear) + "/"
						+ Integer.toString(mMonth) + "/"
						+ Integer.toString(mDay);
				if (first_name.isEmpty() || last_name.isEmpty()
						|| phone_no.isEmpty() || mobilephone_no.isEmpty()
						|| address.isEmpty() || relation_.isEmpty()) {
					Toast.makeText(getApplicationContext(),
							"Please fill out all fields in form",
							Toast.LENGTH_LONG).show();
				} else {
					insertUserDB(adduser);
					int add_uid = add_id;
					Log.w(TAG, "ID :" + add_uid);
					contact = new BPContactInfo(phone_no, mobilephone_no,
							first_name, last_name, address, date_entered,
							add_uid);
					insertContactInformation(contact);
				}
				if (user_added == true && contact_added == true) {
					Intent i = new Intent(getApplicationContext(),
							LoginActivity.class);
					startActivity(i);
					finish();
				}
			}

		});
	}

	public class myOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			relation_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	private void insertUserDB(BPUserProfile profile) {
		ContentValues cv = new ContentValues();
		cv.put(BPUserTable.COLUMN_USERNAME, profile.getusername());
		cv.put(BPUserTable.COLUMN_PASSWORD, profile.getCurrentPassword());
		cv.put(BPUserTable.COLUMN_FIRSTNAME, profile.getFirstName());
		cv.put(BPUserTable.COLUMN_LASTNAME, profile.getLastName());
		cv.put(BPUserTable.COLUMN_HEIGHT, profile.getHeight());
		cv.put(BPUserTable.COLUMN_INITIALWEIGHT, profile.getWeight());
		cv.put(BPUserTable.COLUMN_GENDER, profile.getGender());
		cv.put(BPUserTable.COLUMN_DATEOFBIRTH, profile.getDateofBirth());
		cv.put(BPUserTable.COLUMN_DATEENTERED, profile.getDateEntered());
		userUri = getContentResolver().insert(userTable, cv);
		String user_id = userUri.getPathSegments().get(1);
		int id = Integer.parseInt(user_id);
		add_id = id;
		Log.w(TAG, "UserUri:" + userUri);
		cv.clear();
		user_added = true;
	}

	private void insertContactInformation(BPContactInfo info) {
		ContentValues cv = new ContentValues();
		cv.put(BPContactInformationTable.COLUMN_PERSONALPHONE,
				info.getPersonPhone());
		cv.put(BPContactInformationTable.COLUMN_HOMEPHONE, info.gethomePhone());
		cv.put(BPContactInformationTable.COLUMN_FIRSTNAME, info.getFirstName());
		cv.put(BPContactInformationTable.COLUMN_LASTNAME, info.getLastName());
		cv.put(BPContactInformationTable.COLUMN_ADDRESS, info.getAddress());
		cv.put(BPContactInformationTable.COLUMN_DATEENTERED, info.getDate());
		cv.put(BPContactInformationTable.COLUMN_USERID, info.getUserId());
		getContentResolver().insert(contactInformation, cv);
		cv.clear();
		contact_added = true;
	}

}
