package cs.ecl.bp.app;

import org.achartengine.GraphicalActivity;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;

public class GraphLayout extends GraphicalActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		

	}
	
	// adds title bar button functionality
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {
			case (android.R.id.home):
				finish();
				break;
			}

			return super.onOptionsItemSelected(item);

		}

}
