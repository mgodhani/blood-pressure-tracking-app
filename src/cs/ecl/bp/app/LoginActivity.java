package cs.ecl.bp.app;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.database.Cursor;
import cs.ecl.bp.app.contentprovider.BPContentProvider;
import cs.ecl.bp.app.database.BPUserTable;
import cs.ecl.bp.app.MainActivity;

public class LoginActivity extends Activity {

	EditText username, password;

	Button loginbtn;
	Button registerbtn;

	MainActivity add_user;

	SessionManagement session;
	String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
	Uri userTable = Uri.parse(AUTH + BPUserTable.TABLE_NAME);
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		// Session Manager
		session = new SessionManagement(getApplicationContext());

		// Email, Password input text
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);

		//Toast.makeText(getApplicationContext(),"User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();

		// Login button
		loginbtn = (Button) findViewById(R.id.login);

		// Register button
		registerbtn = (Button) findViewById(R.id.register);

		// Login button click event
		loginbtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get username, password from EditText
				String user_name = username.getText().toString();
				String password_login = password.getText().toString();
				String input_password = null;
				String user_id = null;
				String input_username = null;

				if (!password_login.isEmpty()) {
					Cursor cl = getContentResolver().query(
							userTable,
							null,
							BPUserTable.COLUMN_USERNAME + "=" + "'" + user_name
									+ "'", null, null);
					if (cl.moveToFirst()) {
						do {
							input_password = cl.getString(cl
									.getColumnIndex(BPUserTable.COLUMN_PASSWORD));
							user_id = cl.getString(cl
									.getColumnIndex(BPUserTable.COLUMN_ID));
							input_username = cl.getString(cl
									.getColumnIndex(BPUserTable.COLUMN_USERNAME));

						} while (cl.moveToNext());
					}
				}

				// Check if username, password is filled
				if (user_name.trim().length() > 0
						&& password_login.trim().length() > 0) {

					if (user_name.equals(input_username)
							&& password_login.equals(input_password)) {

						session.createLoginSession(user_name, user_id);

						// Starting MainActivity
						Intent i = new Intent(getApplicationContext(),
								MainActivity.class);
						startActivity(i);
						finish();

					} else {
						// username / password doesn't match
						alert.showAlertDialog(LoginActivity.this,
								"Login failed..",
								"Username/Password is incorrect", false);
					}
				} else {
					// user didn't entered username or password
					// Show alert asking him to enter the details
					alert.showAlertDialog(LoginActivity.this, "Login failed..",
							"Please enter username and password", false);
				}

			}
		});

		registerbtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0){
				Intent i = new Intent(getApplicationContext(),RegisterActivity.class);
				startActivity(i);
				finish();
			}
			
		});

	}
	
	@Override
    public void onBackPressed() {
        super.onBackPressed();   
        Intent i = new Intent(getApplicationContext(),LoginActivity.class);
		startActivity(i);
		finish();
    }
}
