package cs.ecl.bp.app;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cs.ecl.bp.app.contentprovider.BPContentProvider;
import cs.ecl.bp.app.database.BPContactInformationTable;
import cs.ecl.bp.app.database.BPHealthProviderTable;
import cs.ecl.bp.app.database.BPUserDataTable;
import cs.ecl.bp.app.database.BPUserTable;

public class MainActivity extends Activity {

	String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
	Uri userTable = Uri.parse(AUTH + BPUserTable.TABLE_NAME);
	Uri healthProviderTable = Uri
			.parse(AUTH + BPHealthProviderTable.TABLE_NAME);
	Uri contactInformation = Uri.parse(AUTH
			+ BPContactInformationTable.TABLE_NAME);
	Uri userDataTable = Uri.parse(AUTH + BPUserDataTable.TABLE_NAME);
	Uri allTables = Uri.parse(AUTH + BPUserTable.TABLE_NAME + "/"
			+ BPHealthProviderTable.TABLE_NAME + "/"
			+ BPContactInformationTable.TABLE_NAME + "/"
			+ BPUserDataTable.TABLE_NAME);

	Cursor dbCursor;
	ArrayList<Integer> userIds;
	ArrayList<LinearLayout> llList;
	SessionManagement session;
	Button logoutbtn;
	
	//keep
	//int average_heartrate = 0;
	//int average_diastolic = 0;
	//int average_systolic = 0;
	LinearLayout ll;
	List<UserDataInstance> temp;
	List<UserDataInstance> currentData;
	
	@SuppressWarnings({ "static-access", "deprecation" })
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		session = new SessionManagement(getApplicationContext());
		session.checkLogin();

		ActionBar bar = getActionBar();
    	
    	bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#132C87")));
		
		//TESTER DATA
		addTesterData();
		
		// Get session details
		HashMap<String, String> user = session.getUserDetails();
		String id = user.get(session.KEY_ID);
		
		Cursor getUser = getContentResolver().query(
				userTable,
				null,
				BPUserTable.COLUMN_ID + "=" + "'" + id
						+ "'", null, null);
		
		//CHECK FOR WARNINGS
		Intent currentIntent = getIntent();
		int theS = currentIntent.getIntExtra("systolic", 2);
		int theD = currentIntent.getIntExtra("diastolic", 2);
		String message = "";
		
		//This will get the user name
        String Username = "N/A";
        
        if (getUser.moveToFirst()) {
                Username = getUser.getString(getUser.getColumnIndex(BPUserTable.COLUMN_FIRSTNAME)) + " "+ (getUser.getString(getUser.getColumnIndex(BPUserTable.COLUMN_LASTNAME)));
        }
		
		// BOTH ARE BAD
		if (theS != 2 && theD != 2){
			boolean hospital = false;
			if (theS == 1){
				message += "Your Systolic Blood Pressure is low, and ";
			}
			else if (theS == 3){
				message += "Your Systolic Blood Pressure is high, and ";
			}
			else if (theS == 4){
				hospital = true;
				message += "Your Systolic Blood Pressure is very high, and ";
			}
			if (theD == 1){
				message += "your Diastolic Blood Pressure is low. ";
			}
			else if (theD == 3){
				message += "your Diastolic Blood Pressure is high. ";
			}
			else if (theD == 4){
				hospital = true;
				message += "your Diastolic Blood Pressure is very high. ";
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
	        alertDialog.setTitle("Regarding your New Entry");
			if (hospital){
				message += "It is highly advised you visit a Hospital.";
				//Message here!
				// Or anywere you wants2
				
				//SmsManager smsManager =     SmsManager.getDefault();
				//smsManager.sendTextMessage("+1416-518-6189", null, "Message", null, null);
				
				sendSMS("4165186189", Username + "'s " + message + ". You should call them.");
				alertDialog.setMessage(message);
				alertDialog.setIcon(R.drawable.hospital);
		        alertDialog.setButton("View Nearby Hospitals", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            	Intent intentMap = new Intent(getApplicationContext(),GoogleActivity.class);
		    			startActivity(intentMap);
		            }
		        });
			}
			else{
				message += "Please consider seeing a doctor.";
				alertDialog.setMessage(message);
				alertDialog.setIcon(R.drawable.doctor);
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            }
		        });
			}
			getIntent().removeExtra("systolic");
			getIntent().removeExtra("diastolic");
			alertDialog.show();
		}
		
		// ONlY SYSTOLIC IS BAD
		else if (theS != 2){
			boolean hospital = false;
			if (theS == 1){
				message += "Your Systolic Blood Pressure is low. ";
			}
			else if (theS == 3){
				message += "Your Systolic Blood Pressure is high. ";
			}
			else if (theS == 4){
				hospital = true;
				message += "Your Systolic Blood Pressure is very high. ";
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
	        alertDialog.setTitle("Regarding your New Entry");
			if (hospital){
				message += "It is highly advised you visit a Hospital.";
				sendSMS("416-518-6189", Username + "'s " + message + ". You should call them.");
				alertDialog.setMessage(message);
				alertDialog.setIcon(R.drawable.hospital);
		        alertDialog.setButton("View Nearby Hospitals", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            	Intent intentMap = new Intent(getApplicationContext(),GoogleActivity.class);
		    			startActivity(intentMap);
		            }
		        });
			}
			else{
				message += "Please consider seeing a doctor.";
				alertDialog.setMessage(message);
				alertDialog.setIcon(R.drawable.doctor);
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            }
		        });
			}
			getIntent().removeExtra("systolic");
			getIntent().removeExtra("diastolic");
			alertDialog.show();
		}
		else if (theD != 2){
			boolean hospital = false;
			if (theD == 1){
				message += "Your Diastolic Blood Pressure is low. ";
			}
			else if (theD == 3){
				message += "Your Diastolic Blood Pressure is high. ";
			}
			else if (theD == 4){
				hospital = true;
				message += "Your Diastolic Blood Pressure is very high. ";
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
	        alertDialog.setTitle("Regarding your New Entry");
			if (hospital){
				message += "It is highly advised you visit a Hospital.";
				sendSMS("416-518-6189", Username + "'s " + message + ". You should call them.");
				alertDialog.setMessage(message);
				alertDialog.setIcon(R.drawable.hospital);
		        alertDialog.setButton("View Nearby Hospitals", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            	Intent intentMap = new Intent(getApplicationContext(),GoogleActivity.class);
		    			startActivity(intentMap);
		            }
		        });
			}
			else{
				message += "Please consider seeing a doctor.";
				alertDialog.setMessage(message);
				alertDialog.setIcon(R.drawable.doctor);
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            }
		        });
			}
			getIntent().removeExtra("systolic");
			getIntent().removeExtra("diastolic");
			alertDialog.show();
		}
		getIntent().removeExtra("systolic");
		getIntent().removeExtra("diastolic");
		
		Cursor getUserData = getContentResolver().query(
				userDataTable,
				null,
				BPUserDataTable.COLUMN_USERID + "=" + "'" + id
						+ "'", null, null);
		
		TextView l_name = (TextView) findViewById(R.id.label_username);
		TextView l_weight = (TextView) findViewById(R.id.label_weight);
		temp = new ArrayList<UserDataInstance>();
		currentData = new ArrayList<UserDataInstance>();
		
		if (getUserData.moveToFirst()) {
			do {
				temp.add(new UserDataInstance(getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_DATEENTERED)),
						 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_HEARTRATE)),
						 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_SYSTOLIC)),
						 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_DIASTOLIC)),
						 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_BLOODPRESSURE)),
						 getUserData.getString(getUserData.getColumnIndex(BPUserDataTable.COLUMN_WEIGHT))));
			} while (getUserData.moveToNext());
		}
		//Sort the list of UDI by date
		Collections.sort(temp, new UDICustomComparator());
		
		if (getUser.moveToFirst()) {
			String weight = getUser.getString(getUser.getColumnIndex(BPUserTable.COLUMN_INITIALWEIGHT));
			l_name.setText("Name: " + getUser.getString(getUser.getColumnIndex(BPUserTable.COLUMN_FIRSTNAME)) + " "+ (getUser.getString(getUser.getColumnIndex(BPUserTable.COLUMN_LASTNAME))));
	
			if (temp.size() != 0){
				l_weight.setText("Weight: " + temp.get(temp.size()-1).getWeight());
			}
			else{
				l_weight.setText("Weight: " + weight);
				System.out.println("weight was found via initial data");
			}
		}
		
		ll  = (LinearLayout) findViewById(R.id.mainlayoutforboxes);
		
		// daily == 1
		generateData(1).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), DetailedActivity.class);
				intent.putExtra("range", 1);
				startActivity(intent);
			}
		});
		// weekly == 2
		generateData(2).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), DetailedActivity.class);
				intent.putExtra("range", 2);
				startActivity(intent);
			}
		});

		// this month == 3
		generateData(3).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), DetailedActivity.class);
				intent.putExtra("range", 3);
				startActivity(intent);
			}
		});

		// last month == 4
		generateData(4).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), DetailedActivity.class);
				intent.putExtra("range", 4);
				startActivity(intent);
			}
		});

		// this year == 5
		generateData(5).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), DetailedActivity.class);
				intent.putExtra("range", 5);
				startActivity(intent);
			}
		});
		
		//lv.setAdapter(adapter);
		
		//Setup tip of the day
		TextView tod = (TextView) findViewById(R.id.tip_of_the_day);
		String todValues[] = {"Maintaining a healthy weight. Being overweight can make you two to six times more likely to develop high blood pressure than if you are at your desirable weight. Even small amounts of weight loss can make a big difference in helping to prevent and treat high blood pressure.",
							  "Getting regular exercise. People who are physically active have a lower risk of getting high blood pressure -- 20% to 50% lower -- than people who are not active. You don't have to be a marathon runner to benefit from physical activity. Even light activities, if done daily, can help lower your risk.",
							  "Reducing salt intake. Often, when people with high blood pressure cut back on salt, their blood pressure falls. Cutting back on salt also prevents blood pressure from rising.",
							  "Drinking alcohol in moderation, if at all. Drinking too much alcohol can raise your blood pressure. To help prevent high blood pressure, limit how much alcohol you drink to no more than two drinks a day. The \"Dietary Guidelines for Americans\" recommends that for overall health, women should limit their alcohol to no more than one drink a day.",
							  "Reduce stress. Stress can make blood pressure go up, and over time may contribute to the cause of high blood pressure. There are many steps you can take to reduce your stress.",
							  "Potassium. Eating foods rich in potassium will help protect some people from developing high blood pressure. You probably can get enough potassium from your diet, so a supplement isn't necessary (and could be dangerous without a doctor's oversight). Many fruits, vegetables, dairy foods, and fish are good sources of potassium.",
							  "Calcium. Populations with low calcium intakes have high rates of high blood pressure. However, it has not been proven that taking calcium tablets will prevent high blood pressure. But it is important to be sure to get at least the recommended amount of calcium from the foods you eat. Dairy foods like low-fat milk, yogurt, and cheese are good sources of calcium."};
		tod.setText("Tip of the Day: " + todValues[rand(todValues.length-1)]);
		
	}
	
    private void sendSMS(String num, String msg) {
            // TODO Auto-generated method stub
    	SmsManager smsManager =     SmsManager.getDefault();
		smsManager.sendTextMessage(num, null, "Message", null, null);
    }
	
	public static int rand(int hi){
			Random rn = new Random();
            int n = hi - 0 + 1;
            int i = rn.nextInt() % n;
            if (i < 0)
                    i = -i;
            return 0 + i;
    }

	public LinearLayout generateData(int x){
		Calendar cal = GregorianCalendar.getInstance();
		Date d = new Date();
		cal.setTime(d);
		int average_heartrate = 0;
		int average_diastolic = 0;
		int average_systolic = 0;
		int average_weight = 0;
		TextView tv;
		LinearLayout rowOfData; 
		String boxRange = "";
		
		//empty list
		currentData.clear();
		
		
		switch (x){
			case 1:
				boxRange = "Today";
				cal.add(GregorianCalendar.DATE, -1);
				for (int i = 0; i < temp.size(); i++){
					if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
						currentData.add(temp.get(i));
					}
				}
				System.out.println("today list builder accessed");
				break;
			case 2:
				boxRange = "This Week";
				cal.add(GregorianCalendar.DATE, -7);
				for (int i = 0; i < temp.size(); i++){
					if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
						currentData.add(temp.get(i));
					}
				}
				break;
			case 3:
				boxRange = "This Month";
				cal.add(GregorianCalendar.MONTH, -1);
				for (int i = 0; i < temp.size(); i++){
					if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
						currentData.add(temp.get(i));
					}
				}
				break;
			case 4:
				boxRange = "Last Month";
				Calendar cal2 = GregorianCalendar.getInstance();
				cal2.setTime(d);
				cal.add(GregorianCalendar.MONTH, -2);
				cal2.add(GregorianCalendar.MONTH, -1);
				for (int i = 0; i < temp.size(); i++){
					if (isWithinRange(temp.get(i).get_date(), cal.getTime(), cal2.getTime())){
						currentData.add(temp.get(i));
					}
				}
				break;
			case 5:
				boxRange = "Last 12 Months";
				cal.add(GregorianCalendar.YEAR, -1);
				for (int i = 0; i < temp.size(); i++){
					if (isWithinRange(temp.get(i).get_date(), cal.getTime(), d)){
						currentData.add(temp.get(i));
					}
				}
				break;
		}
		if (currentData.size() != 0){
			System.out.println(currentData.size() + " is the current data size");
			for (int i = 0; i < currentData.size(); i++){
				average_heartrate += Integer.parseInt(currentData.get(i).getHeartrate().toString());
				average_systolic += Integer.parseInt(currentData.get(i).getSystolic().toString());
				average_diastolic += Integer.parseInt(currentData.get(i).getDiastolic().toString());
				average_weight += Integer.parseInt(currentData.get(i).getValueOfWeight().toString());
			}
			average_heartrate = average_heartrate/currentData.size();
			average_systolic = average_systolic/currentData.size();
			average_diastolic = average_diastolic/currentData.size();
			average_weight = average_weight/currentData.size();
			
			boolean systolicFlag = false;
			boolean diastolicFlag = false;
			
			if (average_systolic > 119 || average_systolic < 90){
				systolicFlag = true;
			}
			if (average_diastolic > 79 || average_diastolic < 60){
				diastolicFlag = true;
			}
			
			/*String sString = "";
			String dString = "";

			if (average_systolic > 119){
				sString = "(high)";
			}
			else if (average_systolic > 89){
				//nothing
				sString = "(normal)";
			}
			else {
				sString = "(low)";
			}
			if (average_diastolic > 79){
				dString = "(high)";
			}
			else if (average_diastolic > 59){
				//nothing
				dString = "(normal)";
			}
			else {
				dString = "(low)";
			}*/
	
			rowOfData = (LinearLayout)getLayoutInflater().inflate(R.layout.record_listview, null);
			TextView tv_title = (TextView) rowOfData.findViewById( R.id.time );
			tv_title.setText(boxRange);
			TextView tv_bp = (TextView) rowOfData.findViewById( R.id.values );
			if (diastolicFlag || systolicFlag){
				tv_bp.setTextColor(Color.RED);
			}
			tv_bp.setText(average_systolic + "/" + average_diastolic);
			TextView tv_weight = (TextView) rowOfData.findViewById( R.id.weight );
			tv_weight.setText(average_weight + "");
			TextView tv_heartrate = (TextView) rowOfData.findViewById( R.id.pulse );
			tv_heartrate.setText(average_heartrate + "");
			
			//tv.setText(tv.getText() + "\n\nAverage Heart Rate: " + average_heartrate + " BPM\nAverage Systolic Blood Pressure: " + average_systolic + " mmHg " + sString + "\nAverage Diastolic Blood Pressure: " + average_diastolic + " mmHg " + dString);
		}
		else{
			rowOfData = (LinearLayout)getLayoutInflater().inflate(R.layout.record_listview_nodata, null);
			TextView tv_title = (TextView) rowOfData.findViewById( R.id.time );
			tv_title.setText(boxRange);
		}
		
		rowOfData.setPadding(10,10,10,10);
		rowOfData.setId(x);
		ll.addView(rowOfData);
		
		return rowOfData;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.user_update:
			Intent i = new Intent(getApplicationContext(),
					BPTabLayout.class);
			startActivity(i);
			finish();
			break;
		case R.id.add_data:
			Intent intent = new Intent(getApplicationContext(),AddBPActivity.class);
			startActivity(intent);
			break;
		case R.id.locate_hospital:
			Intent intentMap = new Intent(getApplicationContext(),GoogleActivity.class);
			startActivity(intentMap);
			break;
		case R.id.logout_menu:
			session.logoutUser();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	boolean isWithinRange(Date testDate, Date startDate, Date endDate) {
		return !(testDate.before(startDate) || testDate.after(endDate));
	}
	
	void addTesterData(){
		String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
		Uri userTable = Uri.parse(AUTH + BPUserTable.TABLE_NAME);
		Uri healthProviderTable = Uri
				.parse(AUTH + BPHealthProviderTable.TABLE_NAME);
		Uri contactInformation = Uri.parse(AUTH
				+ BPContactInformationTable.TABLE_NAME);
		Uri userDataTable = Uri.parse(AUTH + BPUserDataTable.TABLE_NAME);
		Uri allTables = Uri.parse(AUTH + BPUserTable.TABLE_NAME + "/"
				+ BPHealthProviderTable.TABLE_NAME + "/"
				+ BPContactInformationTable.TABLE_NAME + "/"
				+ BPUserDataTable.TABLE_NAME);

		Cursor dbCursor;
		
		if(getContentResolver().query(userTable, null, "1=1", null, null).getCount() == 0) {
			insertUsers();
		}

		if(getContentResolver().query(contactInformation, null, "1=1", null, null).getCount() == 0) {
			insertContactInformation();
		}
		
		if(getContentResolver().query(userDataTable, null, "1=1", null, null).getCount() == 0) {
			insertUserData();
			Toast.makeText(this, "Note: The user account hfazal/123456 has preset data in it", Toast.LENGTH_LONG).show();
		}
	}
	
	private void insertUsers() {
		ContentValues cv = new ContentValues();
		cv.put(BPUserTable.COLUMN_USERNAME, "hfazal");
		cv.put(BPUserTable.COLUMN_PASSWORD, "123456");
		cv.put(BPUserTable.COLUMN_FIRSTNAME, "Husain");
		cv.put(BPUserTable.COLUMN_LASTNAME, "Fazal");
		cv.put(BPUserTable.COLUMN_HEIGHT, "6ft");
		cv.put(BPUserTable.COLUMN_INITIALWEIGHT, "160lbs");
		cv.put(BPUserTable.COLUMN_GENDER, "Male");
		cv.put(BPUserTable.COLUMN_DATEOFBIRTH, "01/01/1970");
		cv.put(BPUserDataTable.COLUMN_DATEENTERED, "2011/03/24");
		Uri userUri = getContentResolver().insert(userTable, cv);
		userIds = new ArrayList<Integer>();
		userIds.add(Integer.valueOf(userUri.getLastPathSegment()));
		cv.clear();
	}

	private void insertContactInformation() {
		ContentValues cv = new ContentValues();
		cv.put(BPContactInformationTable.COLUMN_PERSONALPHONE, "416444444");
		cv.put(BPContactInformationTable.COLUMN_HOMEPHONE, "6476666666");
		cv.put(BPContactInformationTable.COLUMN_FIRSTNAME, "Yasmin");
		cv.put(BPContactInformationTable.COLUMN_LASTNAME, "Fazal");
		cv.put(BPContactInformationTable.COLUMN_ADDRESS, "1 Lepus Starway, Toronto ON");
		cv.put(BPUserDataTable.COLUMN_DATEENTERED, "2011/03/24");
		cv.put(BPContactInformationTable.COLUMN_USERID, userIds.get(0));
		getContentResolver().insert(contactInformation, cv);
		cv.clear();
	}

	private void insertUserData() {
		ContentValues cv = new ContentValues();
		cv.put(BPUserDataTable.COLUMN_SYSTOLIC, "95");
		cv.put(BPUserDataTable.COLUMN_DIASTOLIC, "55");
		cv.put(BPUserDataTable.COLUMN_HEARTRATE, "90");
		cv.put(BPUserDataTable.COLUMN_WEIGHT, "161lbs");
		cv.put(BPUserDataTable.COLUMN_BODYPART, "Right Arm");
		cv.put(BPUserDataTable.COLUMN_POSTURE, "Standing");
		cv.put(BPUserDataTable.COLUMN_BLOODPRESSURE, "0");
		cv.put(BPUserDataTable.COLUMN_COMMENT, "After Doctor Visit");
		cv.put(BPUserDataTable.COLUMN_RECOMMENDATION, "(nothing)");
		cv.put(BPUserDataTable.COLUMN_DATEENTERED, "2013/03/24");
		cv.put(BPUserDataTable.COLUMN_USERID, userIds.get(0));
		getContentResolver().insert(userDataTable, cv);
		cv.clear();
		
		cv.put(BPUserDataTable.COLUMN_SYSTOLIC, "105");
		cv.put(BPUserDataTable.COLUMN_DIASTOLIC, "70");
		cv.put(BPUserDataTable.COLUMN_HEARTRATE, "92");
		cv.put(BPUserDataTable.COLUMN_WEIGHT, "162lbs");
		cv.put(BPUserDataTable.COLUMN_BODYPART, "Right Arm");
		cv.put(BPUserDataTable.COLUMN_POSTURE, "Standing");
		cv.put(BPUserDataTable.COLUMN_BLOODPRESSURE, "0");
		cv.put(BPUserDataTable.COLUMN_COMMENT, "After Working out");
		cv.put(BPUserDataTable.COLUMN_RECOMMENDATION, "(nothing)");
		cv.put(BPUserDataTable.COLUMN_DATEENTERED, "2013/02/24");
		cv.put(BPUserDataTable.COLUMN_USERID, userIds.get(0));
		getContentResolver().insert(userDataTable, cv);
		cv.clear();
		
		cv.put(BPUserDataTable.COLUMN_SYSTOLIC, "100");
		cv.put(BPUserDataTable.COLUMN_DIASTOLIC, "80");
		cv.put(BPUserDataTable.COLUMN_HEARTRATE, "94");
		cv.put(BPUserDataTable.COLUMN_WEIGHT, "162lbs");
		cv.put(BPUserDataTable.COLUMN_BODYPART, "Right Arm");
		cv.put(BPUserDataTable.COLUMN_POSTURE, "Standing");
		cv.put(BPUserDataTable.COLUMN_BLOODPRESSURE, "0");
		cv.put(BPUserDataTable.COLUMN_COMMENT, "After Working out");
		cv.put(BPUserDataTable.COLUMN_RECOMMENDATION, "(nothing)");
		cv.put(BPUserDataTable.COLUMN_DATEENTERED, "2013/04/02");
		cv.put(BPUserDataTable.COLUMN_USERID, userIds.get(0));
		getContentResolver().insert(userDataTable, cv);
		cv.clear();
	}

}
