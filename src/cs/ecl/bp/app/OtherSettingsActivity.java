package cs.ecl.bp.app;

import java.util.HashMap;

import cs.ecl.bp.app.contentprovider.BPContentProvider;
import cs.ecl.bp.app.database.BPUserDataTable;
import cs.ecl.bp.app.database.BPUserTable;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OtherSettingsActivity extends Activity {
	
	String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
	Uri userTable = Uri.parse(AUTH + BPUserTable.TABLE_NAME);
	Button wipe;
	int id;
	SessionManagement session;
	AlertDialog.Builder builder;
	final Context context = this;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.othersettings);
        
        session = new SessionManagement(getApplicationContext());
		session.checkLogin();
        
     // Get session details
     		HashMap<String, String> user = session.getUserDetails();
     		// String name = user.get(session.KEY_NAME);
     		id = Integer.parseInt(user.get(SessionManagement.KEY_ID));
     		
        wipe = (Button) findViewById(R.id.wipe_data);
        wipe.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		switch (v.getId()) {
        		case R.id.wipe_data:
        			 builder = new AlertDialog.Builder(context);
        			 builder.setMessage("Are you sure you want to delete?")
        			        .setCancelable(false)
        			        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
        			            public void onClick(DialogInterface dialog, int id) {
        			            	getContentResolver().delete(userTable,BPUserDataTable.COLUMN_ID + "=" + id, null);
        		        			Intent i = new Intent(getApplicationContext(),
        									LoginActivity.class);
        							startActivity(i);
        							finish();
        			            }
        			        })
        			        .setNegativeButton("No", new DialogInterface.OnClickListener() {
        			            public void onClick(DialogInterface dialog, int id) {
        			                 dialog.cancel();
        			            }
        			        });
        			 AlertDialog alert = builder.create();
        			 alert.show();
        		}

        	}
        	
        });
	}

}
