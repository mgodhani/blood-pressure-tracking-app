package cs.ecl.bp.app;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import cs.ecl.bp.app.datastorage.BPUserProfile;
import android.util.Log;

public class RegisterActivity extends Activity {

	EditText firstname;
	EditText lastname;
	EditText height;
	EditText weight;
	Spinner height_measurement;
	Spinner weight_measurement;
	EditText dob;
	EditText username;
	EditText password;
	Spinner Gender;
	Button submit;
	Button addemergencycontact;
	public static BPUserProfile insert_user;
	String gender_type;
	String height_type;
	String weight_type;
	private int year;
	private int month;
	private int day;

	static final int DATE_DIALOG_ID = 999;

	private static final String TAG = "registeract";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);

		firstname = (EditText) findViewById(R.id.firstname_add);
		lastname = (EditText) findViewById(R.id.lastname_add);
		height = (EditText) findViewById(R.id.height_add);
		weight = (EditText) findViewById(R.id.weight_add);
		dob = (EditText) findViewById(R.id.dob_add);
		dob.setClickable(true);
		dob.setOnClickListener(new View.OnClickListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});
		Gender = (Spinner) findViewById(R.id.gender_add);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.gender_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Gender.setAdapter(adapter);
		Gender.setOnItemSelectedListener(new genderOnItemSelectedListener());
		username = (EditText) findViewById(R.id.username_add);
		password = (EditText) findViewById(R.id.password_add);
		addemergencycontact = (Button) findViewById(R.id.emergency);

		height_measurement = (Spinner) findViewById(R.id.height_measure_add);
		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(
				this, R.array.height_array,
				android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		height_measurement.setAdapter(adapter2);
		height_measurement
				.setOnItemSelectedListener(new heightOnItemSelectedListener());

		weight_measurement = (Spinner) findViewById(R.id.weight_measure_add);
		ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(
				this, R.array.weight_array,
				android.R.layout.simple_spinner_item);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		weight_measurement.setAdapter(adapter3);
		weight_measurement
				.setOnItemSelectedListener(new weightOnItemSelectedListener());

		addemergencycontact.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				boolean redirect = false;
				String first_name = firstname.getText().toString();
				Log.w(TAG, "First Name :" + first_name);
				String last_name = lastname.getText().toString();
				Log.w(TAG, "Last Name :" + last_name);
				String user_name = username.getText().toString();
				Log.w(TAG, "Username :" + user_name);
				String password_ = password.getText().toString();
				Log.w(TAG, "Password :" + password_);
				String gender = gender_type;
				Log.w(TAG, "Gender :" + gender);

				String height_;
				if (height_type.matches("inches")) {
					height_ = height.getText().toString() + " inch";
				} else {
					height_ = height.getText().toString() + " feet";
					Log.w(TAG, "Height :" + height_);
				}

				String weight_;
				if (weight_type.matches("kg")) {
					weight_ = weight.getText().toString() + "kg";
				} else {
					weight_ = weight.getText().toString() + "lbs";
				}
				Log.w(TAG, "Weight :" + weight_);
				String dob_ = dob.getText().toString();
				Log.w(TAG, "Dob:" + dob_);
				Calendar today = Calendar.getInstance();
				int mYear = today.get(Calendar.YEAR);
				int mMonth = today.get(Calendar.MONTH);
				int mDay = today.get(Calendar.DAY_OF_MONTH);
				String date_entered = Integer.toString(mYear) + "/"
						+ Integer.toString(mMonth) + "/"
						+ Integer.toString(mDay);
				Log.w(TAG, "Date Entered:" + date_entered);

				if (first_name.isEmpty() || last_name.isEmpty()
						|| user_name.isEmpty() || password_.isEmpty()
						|| gender.isEmpty() || height_.isEmpty()
						|| weight_.isEmpty() || dob_.isEmpty()) {
					Toast.makeText(getApplicationContext(),
							"Please fill out all fields in form",
							Toast.LENGTH_LONG).show();
					redirect = false;
				} else {
					insert_user = new BPUserProfile(first_name, last_name,
							user_name, password_, gender, height_, weight_,
							dob_, date_entered);
					redirect = true;
				}
				if (redirect) {
					Intent i = new Intent(getApplicationContext(),
							EmergencyContactActivity.class);
					startActivity(i);
					finish();
				}
			}

		});

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent i = new Intent(getApplicationContext(), LoginActivity.class);
		startActivity(i);
		finish();
	}

	public class genderOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			gender_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	public class heightOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			height_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	public class weightOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			weight_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			year = 1950;
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			// TODO Auto-generated method stub
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
			dob.setText(new StringBuilder().append(year + 1).append("/")
					.append(month).append("/").append(day).append(" "));

		}
	};

}
