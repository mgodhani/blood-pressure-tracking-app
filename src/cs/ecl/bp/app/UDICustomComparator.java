package cs.ecl.bp.app;

import java.util.Comparator;

public class UDICustomComparator implements Comparator<UserDataInstance> {
	@Override
	public int compare(UserDataInstance arg0, UserDataInstance arg1) {
		return arg0.get_date().compareTo(arg1.get_date());
	}
}