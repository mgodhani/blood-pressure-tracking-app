package cs.ecl.bp.app;

import java.util.Calendar;
import java.util.HashMap;

import cs.ecl.bp.app.contentprovider.BPContentProvider;
import cs.ecl.bp.app.database.BPContactInformationTable;
import cs.ecl.bp.app.datastorage.BPContactInfo;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class UpdateEmergencyContact extends Activity {

	String AUTH = "content://" + BPContentProvider.AUTHORITY + "/";
	Uri contactInformation = Uri.parse(AUTH
			+ BPContactInformationTable.TABLE_NAME);
	Uri userUri;
	SessionManagement session;
	Button submit;
	EditText firstname_emerg;
	EditText lastname_emerg;
	EditText phoneno_emerg;
	EditText mobileno_emerg;
	EditText address_emerg;
	Spinner Relation;
	String relation_type;
	public static int id;
	private static final String TAG = "emergencyact";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.emergency_update);
	
		session = new SessionManagement(getApplicationContext());
		session.checkLogin();

		// Get session details
		HashMap<String, String> user = session.getUserDetails();
		// String name = user.get(session.KEY_NAME);
		id = Integer.parseInt(user.get(SessionManagement.KEY_ID));

		submit = (Button) findViewById(R.id.update_emergency);

		firstname_emerg = (EditText) findViewById(R.id.firstname_emerg_update);
		lastname_emerg = (EditText) findViewById(R.id.lastname_emerg_update);
		phoneno_emerg = (EditText) findViewById(R.id.phoneno_emerg_update);
		mobileno_emerg = (EditText) findViewById(R.id.mobileno_emerg_update);
		address_emerg = (EditText) findViewById(R.id.address_emerg_update);

		Relation = (Spinner) findViewById(R.id.relation_update_emerg);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.relation_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Relation.setAdapter(adapter);
		Relation.setOnItemSelectedListener(new relationOnItemSelectedListener());

		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String first_name = firstname_emerg.getText().toString();
				Log.w(TAG, "First Name :" + first_name);
				String last_name = lastname_emerg.getText().toString();
				Log.w(TAG, "Last Name :" + last_name);
				String phone_no = phoneno_emerg.getText().toString();
				Log.w(TAG, "Phone No :" + phone_no);
				String mobilephone_no = mobileno_emerg.getText().toString();
				Log.w(TAG, "Mobile Phone :" + mobilephone_no);
				String address = address_emerg.getText().toString();
				Log.w(TAG, "Address :" + address);
				String relation_ = relation_type;
				Log.w(TAG, "Relation :" + relation_);
				Calendar today = Calendar.getInstance();
				int mYear = today.get(Calendar.YEAR);
				int mMonth = today.get(Calendar.MONTH);
				int mDay = today.get(Calendar.DAY_OF_MONTH);
				String date_entered = Integer.toString(mYear) + "/"
						+ Integer.toString(mMonth) + "/"
						+ Integer.toString(mDay);
				if (first_name.isEmpty() || last_name.isEmpty()
						|| phone_no.isEmpty() || mobilephone_no.isEmpty()
						|| address.isEmpty() || relation_.isEmpty()) {
					Toast.makeText(getApplicationContext(),
							"Please fill out all fields in form",
							Toast.LENGTH_LONG).show();
				} else {
					BPContactInfo contact = new BPContactInfo(phone_no,
							mobilephone_no, first_name, last_name, address,
							date_entered, id);
					updateContactInformation(contact);
					Intent i = new Intent(getApplicationContext(),
							MainActivity.class);
					startActivity(i);
					finish();
				}
			}

		});
	}

	public class relationOnItemSelectedListener implements
			OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			relation_type = parent.getItemAtPosition(pos).toString();

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	private void updateContactInformation(BPContactInfo info) {
		ContentValues cv = new ContentValues();
		cv.put(BPContactInformationTable.COLUMN_PERSONALPHONE,
				info.getPersonPhone());
		cv.put(BPContactInformationTable.COLUMN_HOMEPHONE, info.gethomePhone());
		cv.put(BPContactInformationTable.COLUMN_FIRSTNAME, info.getFirstName());
		cv.put(BPContactInformationTable.COLUMN_LASTNAME, info.getLastName());
		cv.put(BPContactInformationTable.COLUMN_ADDRESS, info.getAddress());
		cv.put(BPContactInformationTable.COLUMN_DATEENTERED, info.getDate());
		cv.put(BPContactInformationTable.COLUMN_USERID, info.getUserId());
		getContentResolver().update(contactInformation, cv,
				BPContactInformationTable.COLUMN_USERID + "=" + id, null);
		cv.clear();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edituserprofile, menu);
		return true;
	}

}
