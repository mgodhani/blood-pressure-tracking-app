package cs.ecl.bp.app;

import android.annotation.SuppressLint;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class UserDataInstance {
	private Date _date;
	private Number _heartrate;
	private Number _systolic;
	private Number _diastolic;
	private Number _bloodpressure;
	private Number _valueOfWeight;
	private String _weight;
	
	public UserDataInstance(String date, String heartrate, String systolic, String diastolic, String bloodpressure, String weight){
		NumberFormat format = NumberFormat.getInstance(Locale.US);
		try {
			set_date(new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).parse(date));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		try {
			_heartrate = format.parse(heartrate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			_systolic = format.parse(systolic);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			_diastolic = format.parse(diastolic);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			_bloodpressure = format.parse(bloodpressure);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		_weight = weight;
		
		if (_weight.endsWith("lbs")) {
			try {
				_valueOfWeight = format.parse(_weight.substring(0, _weight.length() - 3));
			}catch (ParseException e) {
				e.printStackTrace();
			}
		}
		else if (_weight.endsWith("kg")) {
			try {
				_valueOfWeight = format.parse(_weight.substring(0, _weight.length() - 2));
			}catch (ParseException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public Number getHeartrate() {
		return _heartrate;
	}
	public void setHeartrate(Number heartrate) {
		this._heartrate = heartrate;
	}
	public Number getSystolic() {
		return _systolic;
	}
	public void setSystolic(Number systolic) {
		this._systolic = systolic;
	}
	public Number getDiastolic() {
		return _diastolic;
	}
	public void setDiastolic(Number diastolic) {
		this._diastolic = diastolic;
	}
	public Number getBloodpressure() {
		return _bloodpressure;
	}
	public void setBloodpressure(Number bloodpressure) {
		this._bloodpressure = bloodpressure;
	}
	public String getWeight() {
		return _weight;
	}
	public void setWeight(String weight) {
		this._weight = weight;
	}
	public Number getValueOfWeight() {
		return _valueOfWeight;
	}
	public void setValueOfWeight(Number weight) {
		this._valueOfWeight = weight;
	}
	public Date get_date() {
		return _date;
	}
	public String getStringDate(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy");
		return dateFormat.format(_date);
	}
	public void set_date(Date _date) {
		this._date = _date;
	}
	@SuppressLint("SimpleDateFormat")
	public String toString(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy");
		int s = recommendation(1);
		String s1 = "";
		int d = recommendation(2);
		String d1 = "";
		
		switch (s){
			case 1:
				s1 = "(low)";
				break;
			case 2:
				s1 = "(normal)";
				break;
			case 3:
				s1 = "(high)";
				break;
		}
		switch (d){
			case 1:
				d1 = "(low)";
				break;
			case 2:
				d1 = "(normal)";
				break;
			case 3:
				d1 = "(high)";
				break;
		}
		
		
		return "Values for " + dateFormat.format(_date) + "\n" +
			   "Heart Rate: " + _heartrate + " BPM\n" +
			   "Systolic Blood Pressure: " + _systolic + " mmHg " + s1 + "\n" +
			   "Diastolic Blood Pressure: " + _diastolic + "  mmHg " + d1 + "\n" +
			   "Weight: " + _weight;
	}
	public int recommendation(int x) {
		// 1 == systolic
		// 2 == diastolic
		
		int rc = 0;
		//   Category					1 systolic, mmHg		2 diastolic, mmHg
		// 1 Hypotension				< 90					< 60
		// 2 Desired					90�119					60�79
		// 3 Prehypertension			120�139					80�89
		// 4 Stage 1 Hypertension		140�159					90�99
		// 5 Stage 2 Hypertension		160�179					100�109
		// 6 Hypertensive Crisis		� 180					� 110
		if (x == 1){
			if (_systolic.intValue() > 139){		//very high - hospital
				rc= 4;
			}
			else if (_systolic.intValue() > 119){	//high - doctor
				rc= 3;
			}
			else if (_systolic.intValue() > 89){	//normal
				rc= 2;
			}
			else {									//low - doctor
				rc= 1;
			}
		}
		
		if (x == 2){
			if (_diastolic.intValue() > 89){		//very high - hospital
				rc= 4;
			}
			else if (_diastolic.intValue() > 79){	//high - doctor
				rc= 3;
			}
			else if (_diastolic.intValue() > 59){	//normal
				rc= 2;
			}
			else {									//low - doctor
				rc= 1;
			}
		}
		return rc;
	}
}
