package cs.ecl.bp.app.contentprovider;

import java.util.HashMap;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import cs.ecl.bp.app.SessionManagement;
import cs.ecl.bp.app.database.BPContactInformationTable;
import cs.ecl.bp.app.database.BPDatabaseHelper;
import cs.ecl.bp.app.database.BPHealthProviderTable;
import cs.ecl.bp.app.database.BPUserDataTable;
import cs.ecl.bp.app.database.BPUserTable;

public class BPContentProvider extends ContentProvider {

	private BPDatabaseHelper dbHelper;

	private static final int USER = 1;
	private static final int HEALTHPROVIDER = 2;
	private static final int CONTACTINFORMATION = 3;
	private static final int USERDATA = 4;
	private static final int USER_HEALTHPROVIDER_CONTACTINFORMATION_USERDATA = 5;
	public static final String AUTHORITY = "cs.ecl.bp.app.contentprovider";

	private static final UriMatcher uriMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		uriMatcher.addURI(AUTHORITY, BPUserTable.TABLE_NAME, USER);
		uriMatcher.addURI(AUTHORITY, BPHealthProviderTable.TABLE_NAME,
				HEALTHPROVIDER);
		uriMatcher.addURI(AUTHORITY, BPContactInformationTable.TABLE_NAME,
				CONTACTINFORMATION);
		uriMatcher.addURI(AUTHORITY, BPUserDataTable.TABLE_NAME, USERDATA);
		uriMatcher.addURI(AUTHORITY, BPUserTable.TABLE_NAME + "/"
				+ BPHealthProviderTable.TABLE_NAME + "/"
				+ BPContactInformationTable.TABLE_NAME + "/"
				+ BPUserDataTable.TABLE_NAME,
				USER_HEALTHPROVIDER_CONTACTINFORMATION_USERDATA);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = uriMatcher.match(uri);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int deleted = 0;

		switch (uriType) {
		case USER:
			deleted = db.delete(BPUserTable.TABLE_NAME, selection,
					selectionArgs);
			break;
		case HEALTHPROVIDER:
			deleted = db.delete(BPHealthProviderTable.TABLE_NAME, selection,
					selectionArgs);
			break;
		case CONTACTINFORMATION:
			deleted = db.delete(BPContactInformationTable.TABLE_NAME,
					selection, selectionArgs);
			break;
		case USERDATA:
			deleted = db.delete(BPUserDataTable.TABLE_NAME, selection,
					selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI in delete method: "
					+ uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);

		return deleted;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = uriMatcher.match(uri);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		long id = 0;

		switch (uriType) {
		case USER:
			id = db.insert(BPUserTable.TABLE_NAME, null, values);
			break;
		case HEALTHPROVIDER:
			id = db.insert(BPHealthProviderTable.TABLE_NAME, null, values);
			break;
		case CONTACTINFORMATION:
			id = db.insert(BPContactInformationTable.TABLE_NAME, null, values);
			break;
		case USERDATA:
			id = db.insert(BPUserDataTable.TABLE_NAME, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI in insert method: "
					+ uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);

		return Uri.parse(uri + "/" + String.valueOf(id));
	}

	@Override
	public boolean onCreate() {
		dbHelper = new BPDatabaseHelper(getContext());
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.execSQL("PRAGMA foreign_keys = ON;");
		db.close();
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder qBuilder = new SQLiteQueryBuilder();
		qBuilder.setStrict(false);
		int uriType = uriMatcher.match(uri);

		switch (uriType) {
		case USER:
			qBuilder.setTables(BPUserTable.TABLE_NAME);
			qBuilder.appendWhere(selection);
			break;
		case HEALTHPROVIDER:
			qBuilder.setTables(BPHealthProviderTable.TABLE_NAME);
			qBuilder.appendWhere(selection);
			break;
		case CONTACTINFORMATION:
			qBuilder.setTables(BPContactInformationTable.TABLE_NAME);
			qBuilder.appendWhere(selection);
			break;
		case USERDATA:
			qBuilder.setTables(BPUserDataTable.TABLE_NAME);
			qBuilder.appendWhere(selection);
			break;
		case USER_HEALTHPROVIDER_CONTACTINFORMATION_USERDATA:
			qBuilder.setTables(BPUserTable.TABLE_NAME + ", "
					+ BPHealthProviderTable.TABLE_NAME + ", "
					+ BPContactInformationTable.TABLE_NAME + ", "
					+ BPUserDataTable.TABLE_NAME);
			qBuilder.appendWhere(selection);
			qBuilder.setDistinct(true);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI in query method: "
					+ uri);
		}
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		Cursor cursor = qBuilder.query(db, projection, selection,
				selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);

		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = uriMatcher.match(uri);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int updated = 0;

		switch (uriType) {
		case USER:
			updated = db.update(BPUserTable.TABLE_NAME, values, selection,
					selectionArgs);
			break;
		case HEALTHPROVIDER:
			updated = db.update(BPHealthProviderTable.TABLE_NAME, values,
					selection, selectionArgs);
			break;
		case CONTACTINFORMATION:
			updated = db.update(BPContactInformationTable.TABLE_NAME, values,
					selection, selectionArgs);
			break;
		case USERDATA:
			updated = db.update(BPUserDataTable.TABLE_NAME, values, selection,
					selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI in update method: "
					+ uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);

		return updated;
	}

}
