package cs.ecl.bp.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BPDatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "BPApp.db";
	private static final int DATABASE_VERSION = 1;

	public BPDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		BPUserTable.onCreate(db);
		BPHealthProviderTable.onCreate(db);
		BPContactInformationTable.onCreate(db);
		BPUserDataTable.onCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		BPUserTable.onUpgrade(db);
		BPHealthProviderTable.onUpgrade(db);
		BPContactInformationTable.onUpgrade(db);
		BPUserDataTable.onUpgrade(db);
	}

}
