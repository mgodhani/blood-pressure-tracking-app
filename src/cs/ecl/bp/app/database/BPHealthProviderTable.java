package cs.ecl.bp.app.database;

import android.database.sqlite.SQLiteDatabase;

public class BPHealthProviderTable {

	public static final String TABLE_NAME = "healthprovider";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_FIRSTNAME = "firstname";
	public static final String COLUMN_LASTNAME = "lastname";
	public static final String COLUMN_ADDRESS = "address";
	public static final String COLUMN_PHONE = "phone";
	public static final String COLUMN_DATEENTERED = "dateentered";
	public static final String COLUMN_USERID = "userid";
	
	public static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME
			+ " ("
			+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_FIRSTNAME + " TEXT NOT NULL, "
			+ COLUMN_LASTNAME + " TEXT NOT NULL, "
			+ COLUMN_ADDRESS + " TEXT NOT NULL, "
			+ COLUMN_PHONE + " TEXT NOT NULL, "
			+ COLUMN_DATEENTERED + " TEXT NOT NULL, "
			+ COLUMN_USERID + " INTEGER REFERENCES " + BPUserTable.TABLE_NAME + " (" + BPUserTable.COLUMN_ID + ") ON DELETE CASCADE ON UPDATE CASCADE"
			+ ");";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}
	
}
