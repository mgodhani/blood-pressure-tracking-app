package cs.ecl.bp.app.database;

import android.database.sqlite.SQLiteDatabase;

public class BPUserDataTable {

	public static final String TABLE_NAME = "userdata";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_SYSTOLIC = "systolic";
	public static final String COLUMN_DIASTOLIC = "diastolic";
	public static final String COLUMN_HEARTRATE = "heartrate";
	public static final String COLUMN_WEIGHT = "weight";
	public static final String COLUMN_BODYPART = "bodypart";
	public static final String COLUMN_POSTURE = "posture";
	public static final String COLUMN_BLOODPRESSURE = "bloodpressure";
	public static final String COLUMN_COMMENT = "specialcomments";
	public static final String COLUMN_RECOMMENDATION = "recomendation";
	public static final String COLUMN_DATEENTERED = "dateentered";
	public static final String COLUMN_USERID = "userid";
	
	public static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME
			+ " ("
			+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_SYSTOLIC + " TEXT NOT NULL, "
			+ COLUMN_DIASTOLIC + " TEXT NOT NULL, "
			+ COLUMN_HEARTRATE + " TEXT NOT NULL, "
			+ COLUMN_WEIGHT + " TEXT, "
			+ COLUMN_BODYPART + " TEXT NOT NULL, "
			+ COLUMN_POSTURE + " TEXT NOT NULL, "
			+ COLUMN_BLOODPRESSURE + " TEXT NOT NULL, "
			+ COLUMN_COMMENT + " TEXT NOT NULL, "
			+ COLUMN_RECOMMENDATION + " TEXT NOT NULL, "
			+ COLUMN_DATEENTERED + " TEXT NOT NULL, "
			+ COLUMN_USERID + " INTEGER REFERENCES " + BPUserTable.TABLE_NAME + " (" + BPUserTable.COLUMN_ID + ") ON DELETE CASCADE ON UPDATE CASCADE"
			+ ");";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}
	
}
