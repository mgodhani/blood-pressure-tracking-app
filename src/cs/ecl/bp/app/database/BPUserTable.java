package cs.ecl.bp.app.database;

import android.database.sqlite.SQLiteDatabase;

public class BPUserTable {
	
	public static final String TABLE_NAME = "users";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_USERNAME = "username";
	public static final String COLUMN_PASSWORD = "password";
	public static final String COLUMN_FIRSTNAME = "firstname";
	public static final String COLUMN_LASTNAME = "lastname";
	public static final String COLUMN_HEIGHT = "height";
	public static final String COLUMN_INITIALWEIGHT = "initialweight";
	public static final String COLUMN_GENDER = "gender";
	public static final String COLUMN_DATEOFBIRTH = "dob";
	public static final String COLUMN_DATEENTERED = "dateentered";
//	public static final String COLUMN_HEALTHPROVIDERID = "healthproviderid";
//	public static final String COLUMN_CONTACTINFORMATIONID = "contactinformationid";
//	public static final String COLUMN_USERDATAID = "userdataid";
	
	public static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME
			+ " ("
			+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_USERNAME + " TEXT NOT NULL, "
			+ COLUMN_PASSWORD + " TEXT NOT NULL, "
			+ COLUMN_FIRSTNAME + " TEXT NOT NULL, "
			+ COLUMN_LASTNAME + " TEXT NOT NULL, "
			+ COLUMN_HEIGHT + " TEXT, "
			+ COLUMN_INITIALWEIGHT + " TEXT, "
			+ COLUMN_GENDER + " TEXT NOT NULL, "
			+ COLUMN_DATEOFBIRTH + " TEXT NOT NULL, "
			+ COLUMN_DATEENTERED + " TEXT NOT NULL"
//			+ COLUMN_HEALTHPROVIDERID + " INTEGER NOT NULL, "
//			+ COLUMN_CONTACTINFORMATIONID + " INTEGER NOT NULL, "
//			+ COLUMN_USERDATAID + " INTEGER NOT NULL"
			+ " );";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}
	
}
