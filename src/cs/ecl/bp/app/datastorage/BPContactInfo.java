package cs.ecl.bp.app.datastorage;

public class BPContactInfo {
	
	private String personalphone;
	private String homephone;
	private String firstname;
	private String lastname;
	private String address;
	private String date_entered;
	private Integer user_id;
	
	public BPContactInfo(){
		
	}
	
	public BPContactInfo(String personalphone, String homephone, String firstname, String lastname, String address, String date_entered, Integer user_id){
		this.personalphone = personalphone;
		this.homephone = homephone;
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.date_entered = date_entered;
		this.user_id = user_id;
	}
	
	public String getFirstName(){
		return firstname;
	}
	
	public void setFirstName(String firstname){
		this.firstname = firstname;
	}
	
	public String getLastName(){
		return lastname;
	}
	
	public void setLastName(String lastname){
		this.lastname = lastname;
	}
	
	public String getPersonPhone(){
		return personalphone;
	}
	
	public void setPersonalPhone(String personalphone){
		this.personalphone = personalphone;
	}
	
	public String gethomePhone(){
		return homephone;
	}
	
	
	public void setHomePhone(String homephone){
		this.homephone = homephone;
	}
	
	public String getDate(){
		return date_entered;
	}
	
	public Integer getUserId(){
		return user_id;
	}
	
	public void setUserID(Integer id){
		this.user_id = id;
	}
	
	public String getAddress(){
		return address;
	}
	
	public void setAddress(String address){
		this.address = address;
	}

}
