//Data Storage for Health Provider

package cs.ecl.bp.app.datastorage;

public class BPHealthProviderInfo {
	
	//Health Provider Table Values
	private String firstname;
	private String lastname;
	private String address;
	private String phone;
	private String date_entered;
	private String user_id;
	
	public BPHealthProviderInfo(){
	}
	
	public BPHealthProviderInfo(String firstname,String lastname, String address, String phone, String date_entered, String userid){
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.phone = phone;
		this.date_entered = date_entered;
		this.user_id = userid;
	}
	
	//Get First Name of Health Provider
	public String getFirstName(){
		return firstname;
	}
	
	public void setFirstName(String firstname){
		this.firstname = firstname;
	}
	
	//Get Last Name of Health Provider
	public String getLastname(){
		return lastname;
	}
	
	public void setLastName(String lastname){
		this.lastname = lastname;
	}
	
	public String getAddress(){
		return address;
	}
	
	public void setAddress(String address){
		this.address = address;
	}
	
	public String getPhone(){
		return phone;
	}
	
	public void setPhone(String phone){
		this.phone = phone;
	}
	
	public String getDate(){
		return date_entered;
	}
	
	public void setDate(String date_entered){
		this.date_entered = date_entered;
	}
	
	public String userID(){
		return user_id;
	}

}
