//Meet Godhani
//User Health Data data storage file.
package cs.ecl.bp.app.datastorage;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class BPUserData {

	// User Health Data values
	private String systolic;
	private String diastolic;
	private String heartrate;
	private String weight;
	private String bodypart;
	private String posture;
	private String bloodpressure;
	private String comment;
	private String recommendation;
	private String userid;
	private String date_entered;
	private Calendar measuredDate = Calendar.getInstance();
	private Calendar measuredTime = (Calendar) measuredDate.clone();

	private int[] morning = { 6, 11 };
	private int[] afternoon = { 12, 17 };
	private int[] evening = { 18, 23 };
	private int[] night = { 0, 5 };

	public BPUserData() {
	}

	public Boolean IsMorning() {
		return IsPeriodOf(morning);
	}

	public Boolean IsAfternoon() {
		return IsPeriodOf(afternoon);
	}

	public Boolean IsEvening() {
		return IsPeriodOf(evening);
	}

	public Boolean IsNight() {
		return IsPeriodOf(night);
	}

	@SuppressWarnings("deprecation")
	private Boolean IsPeriodOf(int[] startEnd) {
		Calendar temp = measuredTime;
		Date tempdate = temp.getTime();
		if (tempdate.getHours() >= startEnd[0]
				&& tempdate.getHours() < startEnd[1]) {
			return true;
		} else {
			return false;
		}
	}

	public BPUserData(String systolic, String diastolic, String heartrate,String weight, String bodypart, String posture, String comment,String bloodpressure,String dateentered, String userid) {
		super();
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.heartrate = heartrate;
		this.weight = weight;
		this.bodypart = bodypart;
		this.posture = posture;
		this.bloodpressure = bloodpressure;
		this.comment = comment;
		this.userid = userid;
		//this.date_entered = dateentered;
		//Meet: im changing the way dateentered is done becuase i think something is breaking - Husain
		Date d = new Date();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		this.date_entered = dateFormat.format(d.getTime());
		System.out.println(this.date_entered + " << DATE ENTERED");
	}
	
	public BPUserData(String systolic, String diastolic, String heartrate,String weight, String bodypart, String posture, String comment, String bloodpressure,String dateentered) {
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.heartrate = heartrate;
		this.weight = weight;
		this.bodypart = bodypart;
		this.posture = posture;
		this.bloodpressure = bloodpressure;
		this.comment = comment;
		//this.date_entered = dateentered;
		//Meet: im changing the way dateentered is done becuase i think something is breaking - Husain
		Date d = new Date();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		this.date_entered = dateFormat.format(d.getTime());
		System.out.println(this.date_entered + " << DATE ENTERED");
	}

	public String getSystolic() {
		return systolic;
	}

	public String getDiastolic() {
		return diastolic;
	}

	public String getHeartRate() {
		return heartrate;
	}

	public String getWeight() {
		return weight;
	}

	public String getBodyPart() {
		return bodypart;
	}

	public String getPosture() {
		return posture;
	}

	public String getBPComment() {
		return comment;
	}

	public String getBloodPressure() {
		return bloodpressure;
	}

	public String getUserID() {
		return userid;
	}

	public String getDateEntered() {
		return date_entered;
	}
	
	public void setRecommendation(String recommend){
		this.recommendation = recommend;
	}

	// Set Functions

	// Set Systolic Pressure
	public void setsysPressure(String spressurevalue) {
		if (spressurevalue.isEmpty()) {
			throw new NullValueException(
					"Systolic blood pressure cannot be blank.");
		} else {
			try {
				int spressure_int = Integer.parseInt(spressurevalue);
				if (spressure_int >= 250 || spressure_int <= 50) {
					throw new ValueOutOfBoundsException(
							"The systolic value is out of range.");
				}
				systolic = Integer.toString(spressure_int);
			} catch (NumberFormatException ne) {
				throw new NumberFormatException(
						"Systolic blood pressure must be an integer.");
			}
		}
	}

	// Set Heart Rate
	public void setHearRate(String hRate) {
		if (hRate.isEmpty()) {
			throw new NullValueException("Heart Rate cannot be blank");
		} else {
			try {
				int hRateInt = Integer.parseInt(hRate);
				if (hRateInt >= 300 || hRateInt <= 0) {
					throw new ValueOutOfBoundsException(
							"HeartRate value is out of range");
				}
				heartrate = Integer.toString(hRateInt);
			} catch (NumberFormatException ne) {
				throw new NumberFormatException(
						"Heart rate must be an integer.");
			}
		}
	}

	public void setWeight(String weight) throws NullValueException,
			NumberFormatException, ValueOutOfBoundsException {
		if (weight.isEmpty()) {
			throw new NullValueException("Weight cannot be blank.");
		} else {
			float weightFloat = 0;
			try {
				weightFloat = Float.parseFloat(weight); // throws
														// NumberFormatException
				weight = Float.toString(weightFloat);
			} catch (NumberFormatException ne) {
				throw new NumberFormatException("Weight must be a float.");
			}
		}
	}

	class NullValueException extends RuntimeException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		NullValueException(String s) {
			super(s);
		}
	}

	class ValueOutOfBoundsException extends RuntimeException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		ValueOutOfBoundsException(String s) {
			super(s);
		}
	}
}
