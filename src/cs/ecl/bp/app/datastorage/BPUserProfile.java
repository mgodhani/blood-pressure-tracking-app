//Comments : Data Storage for User Profile
//Meet Godhani

package cs.ecl.bp.app.datastorage;

public class BPUserProfile {

	// User table values
	private String user_id;
	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private String gender;
	private String initialweight;
	private String height;
	private String date_entered;
	private String dob;

	public BPUserProfile() {

	}

	public BPUserProfile(String id, String firstname, String lastname,
			String username, String password, String gender, String height,
			String initialweight, String date_of_birth, String dateentered) {
		super();
		this.user_id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.gender = gender;
		this.height = height;
		this.initialweight = initialweight;
		this.dob = date_of_birth;
		this.date_entered = dateentered;
	}

	// Constaruct without id
	public BPUserProfile(String firstname, String lastname, String username,
			String password, String gender, String height,
			String initialweight,String date_of_birth, String dateentered) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.gender = gender;
		this.height = height;
		this.initialweight = initialweight;
		this.dob = date_of_birth;
		this.date_entered = dateentered;
	}

	public String getId() {
		return user_id;
	}

	public void setId(String id) {
		this.user_id = id;
	}

	public String getFirstName() {
		return firstname;

	}

	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}

	public String getLastName() {
		return lastname;
	}

	public void setLastName(String lastname) {
		this.lastname = lastname;
	}

	public String getusername() {
		return username;
	}

	public String getCurrentPassword() {
		return password;
	}

	public void setNewPassword(String newPassword) {
		this.password = newPassword;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateofBirth() {
		return dob;
	}
	
	public String getDateEntered() {
		return date_entered;
	}

	public String getWeight() {
		return initialweight;
	}

	public void setWeight(String initialweight) {
		this.initialweight = initialweight;
	}
}
